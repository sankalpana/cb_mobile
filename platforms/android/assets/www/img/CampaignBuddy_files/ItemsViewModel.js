/* ----------------------------------------------------------
   -                  Global Variables                      -
   ---------------------------------------------------------- */

var koBinding;
//minimum amount for checkout shopping cart
/* ----------------------------------------------------------
   -              Knockout Custom Bindings                  -
   ---------------------------------------------------------- */

/* ----------------------------------------------------------
   -                     Startup                            -
   ---------------------------------------------------------- */

$(document).ready(function () {
    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
     function (r) {
         console.log("localization loaded: " + r);
         applyKoBindings();
         myApp.showPreloader(AppMessages.COMN_INF103);
         //showTime();
         updateClock();
         loadItems();
     },
     function (err) {
         console.error("Failed to load locale: " + err);
     });
});

//Device ready event subscribe
document.addEventListener("deviceready", function () {

    checkGPSAvailability(); // start the gps check

    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    navigator.app.backHistory()
}


function applyKoBindings() {
    koBinding = new ItemsViewModel();
    ko.applyBindings(koBinding);
    getActivationData(koBinding);
}

ko.bindingHandlers.f7Chk = {
    init: function (element, valueAccessor) {
        $(element).on('change', function () {
            var observable = valueAccessor();
            observable(this.checked);
        });
    },
    update: function (element, valueAccessor) {
        var observable = valueAccessor();
        if (observable !== undefined) {
            $(element).prop("checked", observable());
            // console.log("has active " + selectedExistingFence().hasActiveDateRange);
        }

    }
};

/* ----------------------------------------------------------
   -                     View Models                        -
   ---------------------------------------------------------- */

function ItemsViewModel() {
    var self = this;

    self.userId = ko.observable();
    self.activationID = ko.observable();
    self.activationName = ko.observable();
    self.activationOutlet = ko.observable();
    self.itemsData = ko.observableArray([]);
    self.attandaceStatus = ko.observable(false);
    self.attandaceStatus.subscribe( function(checkedVal) {
        var action = checkedVal ? "1" : "0";
        myApp.showPreloader(AppMessages.COMN_INF102);
        getLocation(null, 'false', null, null)
            .then(function (position) {
                processAndSendLocationData(position, self.activationID(), self.userId(), action);
            })
            .fail(function (err) {
                processAndSendLocationData(null);
            });
    },this);


}


/* ----------------------------------------------------------
   -                     Functions                          -
   ---------------------------------------------------------- */
function loadItems() {
    getItems(
        function (cb) {
            if (cb.meta.status == 'true') {
                var data = cb.data;
                if(data.length <= 0){
                    myApp.addNotification({
                        title: 'We are sorry!',
                        message: 'You have no activations at the movement.'
                    });
                }else {
                    ko.utils.arrayForEach(data, function (item) {
                        koBinding.itemsData.push(item);
                    });
                }
            }
            myApp.hideIndicator();
            myApp.hidePreloader();
        },
        function (err) {
            myApp.hideIndicator();
            myApp.hidePreloader();
            myApp.alert(err.responseText, AppMessages.COMN_ERR100);
        });
}

function getItems(cb, err) {
    var actID = getValueL('KEY_ACTID');
    var url = getValueL(KEY_APIBASEURL) + "v2/sales/items/"+actID;
    var type = "GET";
    var timeOut = 6000;
    AjaxCall(url, {}, timeOut, type, cb, err, true, true);
}


function getActivationData(vm){
    var actID = getValueL('KEY_ACTID');
    var actName = getValueL('KEY_ACTNAME');
    var actOutlet = getValueL('KEY_ACTOUTLET');
    var userId = getValueL("KEY_USRID");
    vm.activationID(actID);
    vm.activationName(actName);
    vm.activationOutlet(actOutlet);
    vm.userId(userId);
}

//Get the geo location via jQuery promise
var getLocation = function (callback, highAccur, timeout, maxAge) {
    //Deferred object for promise interface
    var deferred = new $.Deferred();

    if ($.isFunction(callback)) {
        deferred.then(callback);
    }

    navigator.geolocation.getCurrentPosition(
        function (position) {
            deferred.resolve(position);
        },
        function (err) {
            deferred.reject(err);
        },
        { enableHighAccuracy: highAccur }
    );

    //{ enableHighAccuracy: highAccur, timeout: timeout, maximumAge: maxAge }

    return deferred.promise();
};

//Process and send location data to the server
function processAndSendLocationData(position, actID, usrId, action) {

        var data = new LocationData();
        data.latitude = checkProperValue(position) ? checkAndGetValue(position.coords.latitude, 0) : null;
        data.longitude = checkProperValue(position) ? checkAndGetValue(position.coords.longitude, 0) : null;
        data.activation_id = actID;
        data.userId = usrId;
        data.at_type = action;

    var url = getValueL(KEY_APIBASEURL) + "v2/attendance";
    var params = data;
    var type = "POST";
    var timeOut = 6000;
    AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        if(data.meta.status == "true"){
            myApp.hidePreloader();
            myApp.alert(AppMessages.ITM_INF100,AppMessages.COMN_INF105);

        }else{
            myApp.hidePreloader();
            myApp.alert(thrownError, AppMessages.COMN_ERR100);
        }
    }, function (xhr, ajaxOptions, thrownError) {
        myApp.hidePreloader();
        myApp.alert(thrownError, AppMessages.COMN_ERR100);
    });

}

//Check and choose the value to return, v: value to check, defV: Default value to return if check fails
function checkAndGetValue(v, defV) {
    return checkEmpty(v) ? v : defV;
}

//Location data sent to the server
var LocationData = function () {

    /* Mandatory */

    //Latitude
    this.latitude = null;
    //Longitude
    this.longitude = null;
    //Activation ID
    this.activation_id = null;
    //User ID
    this.userId = null;
    //Action (Checking || checkout)
    this.at_type = null;

    /* Optional */

}

function showTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('time').innerHTML =
        h + ":" + m + ":" + s;
    var t = setTimeout(showTime, 500);
}

function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function updateClock ( )
{
    var currentTime = new Date ( );

    var currentHours = currentTime.getHours ( );
    var currentMinutes = currentTime.getMinutes ( );
    var currentSeconds = currentTime.getSeconds ( );

    // Pad the minutes and seconds with leading zeros, if required
    currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
    currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

    // Choose either "AM" or "PM" as appropriate
    var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

    // Convert the hours component to 12-hour format if needed
    currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

    // Convert an hours component of "0" to "12"
    currentHours = ( currentHours == 0 ) ? 12 : currentHours;

    // Compose the string for display
    var currentTimeString = currentHours + ":" + currentMinutes + " " + timeOfDay;

    // Update the time display
    document.getElementById("time").innerHTML = currentTimeString;
    var t = setTimeout(updateClock, 500);
}
