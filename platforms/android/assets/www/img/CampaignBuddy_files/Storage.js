//Key for cached WebService API base url
var KEY_APIBASEURL = "APP_APIBASEURL";
//User session key
var KEY_USRSESSON = "USR_SESSON";
//User key
var KEY_USRKEY = "USR_KEY";
//Remember me flag
var KEY_RMBR = "USR_RMBR";
//User name
var KEY_USERNAME = "USR_USERNAME";
//User image
var KEY_USERIMAGE = "USR_IMAGE";
//User id
var KEY_USRID = "USR_ID";
var KEY_AUTH = "USR_AUTH";
var KEY_LOCALE = "USR_LOCALE";
//Activation id
var KEY_ACTID = "ACT_ID";
//Activation name
var KEY_ACTNAME = "ACT_NAME";
//Activation outlet
var KEY_ACTOUTLET = "ACT_OUTLET";

function getValueL(key) {
	return window.localStorage.getItem(key);
}
function setValueL(key, value) {
	window.localStorage.setItem(key, value);
}

function getValueS(key) {
	return window.sessionStorage.getItem(key);
}
function setValueS(key, value) {
	window.sessionStorage.setItem(key, value);
}
