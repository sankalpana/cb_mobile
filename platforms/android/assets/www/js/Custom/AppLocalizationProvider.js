window.AppMessages = null;

document.addEventListener('DOMContentLoaded', function () {
    //Activate string format method mode
    format.extend(String.prototype, {});
});


function loadMessage(locale, cb, err) {
    var c = cb;
    var e = err;
    
    var url = "lng/" + locale + "/" + locale + ".json";
    $.getJSON(url, function (data) {
        window.AppMessages = data;
        if (checkFunction(c)) c("locale");
    })
    .fail(function (error) {
        $.getJSON("lng/en-CA/en-CA.json", function (data) {
            window.AppMessages = data;
            if (checkFunction(c)) c("default");
        })
        .fail(function () {
            if (checkFunction(e)) ("error");
        });

    });
}

//Load localization file, By file name
function loadLocaleFile(locale, file, sCb, eCB) {
    var c = sCb;
    var e = eCB;
    var url = "lng/" + locale + "/" + file + ".json";
    
    $.getJSON(url, function (data) {
        window.AppMessages = data;
        if (checkFunction(c)) 
                c(data);
    })
    .fail(function (error) {
          console.error("Fialed to load loacle file : " + file + ".json, From directory lng/"+locale);
          if(checkFunction(e)){
              e(error);
          }  
    });
}

