/* ----------------------------------------------------------
 -                     Startup                            -
 ---------------------------------------------------------- */
var koBinding;

$(document).ready(function () {

    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            //dev
            setValueL(KEY_APIBASEURL, "http://dev.campaignbuddy.biz/api/public/");
            //setValueL(KEY_APIIMAGEUPLOADURL, "http://dev.campaignbuddy.biz/uploader/upload.php");
            //live
            //setValueL(KEY_APIBASEURL, "http://api.campaignbuddy.biz/new/public/");
            setValueL(KEY_APIIMAGEUPLOADURL, "http://uploader.campaignbuddy.biz/upload.php");
            koBinding = new LoginViewModel();
            ko.applyBindings(koBinding);
            var userObj = JSON.parse(getValueL(KEY_USROBJ));
            setTimeout(function () {
                if (isOnline()) {
                    try {
                        if (typeof userObj != "undefined" && checkProperValue(userObj)) {
                            if (userObj.meta.status == "true") {
                                navigator.splashscreen.show();
                                autoLogin();
                            }
                        }
                    }
                    catch (err) {
                        console.log(err);
                        navigateTo("index.html");
                    }
                } else {
                    navigator.splashscreen.hide();
                    myApp.alert(AppMessages.COMN_INF101, AppMessages.COMN_ERR100);
                }
                navigator.splashscreen.hide();
            }, 500);
            //autoLogin();
        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Device ready event subscribe
document.addEventListener("deviceready", function () {

    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    //do nothing
}

ko.bindingHandlers.f7Chk = {
    init: function (element, valueAccessor) {
        // iChanged
        //alert(ko.unwrap(value));
        $(element).on('change', function () {
            var observable = valueAccessor();
            observable(this.checked);
            //alert("init rememberMe " + koBinding.rememberMe());
        });
    },
    update: function (element, valueAccessor) {
        var observable = valueAccessor();
        //alert("update rememberMe " + koBinding.rememberMe());
        $(element).prop("checked", observable());

    }
};

/* ----------------------------------------------------------
 -                     View Models                        -
 ---------------------------------------------------------- */

function LoginViewModel() {
    var self = this;
    self.userName = ko.observable("");
    self.passWord = ko.observable("");
    self.hasUsernameFocus = ko.observable(false);
    self.hasPasswordFocus = ko.observable(false);
    self.enterKeyOnUsername = function (d, e) {
        if (e.keyCode == 13) {
            // if (checkEmpty(self.passWord())){
            //     self.signinBtnClick();
            //     //return false;
            // }                
            // else{
            //     if (checkEmpty(self.userName())) {
            //         self.hasPasswordFocus(true);                
            //     };                
            // }      
            self.hasPasswordFocus(true);
        }
        else {
            return true;
        }
    }
    self.enterKeyOnPassword = function (d, e) {
        if (e.keyCode == 13) {
            self.signinBtnClick();
            return false;
        }
        else {
            return true;
        }
    }
    self.rememberMe = ko.observable(true);
    self.signinBtnClick = function () {
        var validate = checkForEmptyLoginFields(self.userName(), self.passWord());
        if (validate) {
            setValueL(KEY_RMBR, self.rememberMe());
            myApp.showPreloader(AppMessages.LOGIN_INF101);
            logIn(self.userName(), self.passWord());
        }
    }
}

/* ----------------------------------------------------------
 -                     Functions                          -
 ---------------------------------------------------------- */
function autoLogin() {
    var userObjRaw = getValueL(KEY_USROBJ);
    var usrObj = JSON.parse(userObjRaw);
    if (checkEmpty(usrObj)) {
        if (usrObj.meta.status == "true") {
            //myApp.showPreloader(AppMessages.LOGIN_INF101);

            //Successful login
            if (usrObj.data.user_type == '1') {
                // setTimeout(function () {
                    navigateTo("super_home.html");
                // }, 500);
            } else {
                // setTimeout(function () {
                    navigateTo("home.html");
                // }, 500);
            }
        }
    } else {
        navigator.splashscreen.hide();
    }
}

function logIn(uName, pWord) {
    var url = getValueL(KEY_APIBASEURL) + "v2/auth/login";
    //var params = "login=" + uName + "&password=" + pWord;
    var params = {"user_name": uName, "password": pWord};
    var type = "POST";
    var timeOut = 2000;
    if(isOnline()){
        AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
            if (data.meta.status == 'true') {
                if (checkEmpty(data.data.user_type)) {
                    if (data.data.user_type == '0') {
                        setValueL(KEY_USERNAME, data.data.name);
                        setValueL(KEY_USERIMAGE, data.data.image);
                        setValueL(KEY_USRID, data.data.id);
                        setValueL(KEY_USRTYPE, data.data.user_type);
                        var obj = JSON.stringify(data);
                        setValueL(KEY_USROBJ, obj);

                        //Successful login
                        setTimeout(function () {
                            navigateTo("home.html");
                        }, 500);
                    } else if (data.data.user_type == '1') {
                        setValueL(KEY_USERNAME, data.data.name);
                        setValueL(KEY_USERIMAGE, data.data.image);
                        setValueL(KEY_USRID, data.data.id);
                        setValueL(KEY_USRTYPE, data.data.user_type);
                        var obj = JSON.stringify(data);
                        setValueL(KEY_USROBJ, obj);
                        //Successful login
                        setTimeout(function () {
                            navigateTo("super_home.html");
                        }, 500);
                    } else {
                        myApp.hidePreloader();
                        myApp.alert(AppMessages.LOGIN_VAL104, AppMessages.COMN_ERR100);
                    }

                } else {
                    //Clear the sessionStoreage
                    window.sessionStorage.clear();
                    setValueL(KEY_USRSESSON, "");
                    setValueL(KEY_RMBR, "");
                }
            } else if (data.meta.status == 'false') {
                myApp.hidePreloader();
                myApp.alert(AppMessages.LOGIN_VAL102, AppMessages.LOGIN_INF100);
            }
        }, function (xhr, ajaxOptions, thrownError) {
            myApp.hidePreloader();
            myApp.alert(AppMessages.LOGIN_VAL104, AppMessages.COMN_ERR100);
        });
    }else{
        myApp.hidePreloader();
        myApp.alert(AppMessages.COMN_INF101, AppMessages.COMN_ERR100);
    }
}

function checkForEmptyLoginFields(un, pw) {
    var rv = true;

    if (!checkEmpty(un)) {
        rv = false;
        myApp.alert(AppMessages.LOGIN_VAL100, AppMessages.COMN_INF100);
    }
    else if (!checkEmpty(pw)) {
        rv = false;
        myApp.alert(AppMessages.LOGIN_VAL101, AppMessages.COMN_INF100);
    }
    return rv;
}

function checkRemeberMe() {
    var isRememberMe = false;

    if (getValueL(KEY_RMBR) == "true") {
        koBinding.rememberMe(true);
        isRememberMe = true;
    }
    //else {
    //    koBinding.rememberMe(false);
    //    isRememberMe = false;
    //}
    return isRememberMe;
}

