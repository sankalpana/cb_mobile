/* ----------------------------------------------------------
 -                  Global Variables                      -
 ---------------------------------------------------------- */

var koBinding;
//minimum amount for checkout shopping cart
var navFrom = getValueL(KEY_NAVFROM);
/* ----------------------------------------------------------
 -              Knockout Custom Bindings                  -
 ---------------------------------------------------------- */

/* ----------------------------------------------------------
 -                     Startup                            -
 ---------------------------------------------------------- */

$(document).ready(function () {
    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            applyKoBindings();
            if (navFrom == "HOME") {
                myApp.showPreloader(AppMessages.COMN_INF103);
            }
        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Device ready event subscribe
document.addEventListener("deviceready", function () {

    var elm = getValueL(KEY_SELECTED_ITEM_ELEM);
    if (checkProperValue(elm)) {
        var container = $$('.page-content'),
            scrollTo = $$('#' + elm);
        scrollTo['0'].style.backgroundColor = "#AAAAAA";
        //mover scroll posicion, duracion
        container.scrollTop(scrollTo.offset().top - container.offset().top + container.scrollTop(), 1200);
    }

    checkGPSAvailability(); // start the gps check
    tracking();
    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    navigateTo('home.html');
}


function applyKoBindings() {
    koBinding = new ItemsViewModel();
    ko.applyBindings(koBinding);
    myApp.showIndicator();
    loadItems();
    getActivationData(koBinding);
    check_Attendance(koBinding);
}

ko.bindingHandlers.f7Chk = {
    init: function (element, valueAccessor) {
        $(element).on('change', function () {
            var observable = valueAccessor();
            observable(this.checked);
        });
    },
    update: function (element, valueAccessor) {
        var observable = valueAccessor();
        if (observable !== undefined) {
            $(element).prop("checked", observable());
            // console.log("has active " + selectedExistingFence().hasActiveDateRange);
        }

    }
};

/* ----------------------------------------------------------
 -                     View Models                        -
 ---------------------------------------------------------- */

function ItemsViewModel() {
    var self = this;

    self.userId = ko.observable();
    self.activationID = ko.observable();
    self.activationName = ko.observable();
    self.activationOutlet = ko.observable();
    self.itemsData = ko.observableArray([]);
    self.checkedIn = ko.observable();
    self.checkedOut = ko.observable();
    self.attandaceTime = ko.observable('-:-');
    self.attandaceLabel = ko.observable('--');
    self.formattedAttendancce = ko.computed(function () {
        var time = getFormatedTime(self.attandaceTime());
        return time;
    });
    self.pageLoad = ko.observable();
    self.attandaceStatus = ko.observable();
    self.attandaceStatus.subscribe(function (checkedVal) {
        myApp.showPreloader(AppMessages.ITM_INF104);
        var action = checkedVal ? "1" : "0";
        var atndObj = JSON.parse(getValueL(KEY_ATTENDANCE_OBJ));
        if (!self.pageLoad()) {
            self.pageLoad(false);
            //myApp.showPreloader(AppMessages.COMN_INF102);
            getLocation(null, 'false', null, null)
                .then(function (position) {
                    if (!self.checkedIn() && !self.checkedOut()) {
                        //send checkin data
                        myApp.confirm('Do you want to check in?', '', function () {
                            processAndSendLocationData(position, self.activationID(), self.userId(), "1", function (cb) {
                                self.attandaceTime(cb.check_in_time);
                                self.attandaceLabel('checkIn');
                                self.checkedIn(true);
                                self.attandaceStatus(true);
                                if (!isObjectEmpty(atndObj)) {
                                    atndObj.data[0].check_in_time = cb.check_in_time;
                                    setValueL(KEY_ATTENDANCE_OBJ, JSON.stringify(atndObj));
                                }
                            });
                        }, function () {
                            //self.attandaceStatus(false);
                            $('#checkIn').attr('checked', false);
                        });
                    } else if (self.checkedIn() && !self.checkedOut()) {
                        //send checkout data
                        myApp.confirm('Do you want to check out?', '', function () {
                            processAndSendLocationData(position, self.activationID(), self.userId(), "0", function (cb) {
                                self.attandaceTime(cb.check_out_time);
                                self.attandaceLabel('CheckOut');
                                self.attandaceStatus(false);
                                self.checkedIn(true);
                                self.checkedOut(true);
                                $("#checkIn").prop("disabled", true);
                                if (!isObjectEmpty(atndObj)) {
                                    atndObj.data[0].check_in_time = cb.check_out_time;
                                    setValueL(KEY_ATTENDANCE_OBJ, JSON.stringify(atndObj));
                                }
                            });
                        }, function () {
                            //self.attandaceStatus(true);
                            $('#checkIn').attr('checked', true);
                        });
                    } else {
                        self.attandaceStatus(true);
                    }
                    myApp.hidePreloader();

                })
                .fail(function (err) {
                    myApp.hidePreloader();
                    myApp.alert(thrownError, AppMessages.COMN_ERR101);
                });
        } else {
            getLocation(null, 'false', null, null)
                .then(function (position) {
                    if (!self.checkedIn() && !self.checkedOut()) {
                        //send checkin data
                        processAndSendLocationData(position, self.activationID(), self.userId(), "1", function (cb) {
                            self.attandaceTime(cb.check_in_time);
                            self.attandaceLabel('CheckIn');
                            self.checkedIn(true);
                            self.attandaceStatus(true);
                            if (!isObjectEmpty(atndObj)) {
                                atndObj.data[0].check_in_time = cb.check_in_time;
                                setValueL(KEY_ATTENDANCE_OBJ, JSON.stringify(atndObj));
                            }
                        });
                    } else if (self.checkedIn() && !self.checkedOut()) {
                        //send checkout data
                        processAndSendLocationData(position, self.activationID(), self.userId(), "0", function (cb) {
                            self.attandaceTime(cb.check_out_time);
                            self.attandaceLabel('CheckOut');
                            self.attandaceStatus(false);
                            self.checkedIn(true);
                            self.checkedOut(true);
                            $("#checkIn").prop("disabled", true);
                            if (!isObjectEmpty(atndObj)) {
                                atndObj.data[0].check_in_time = cb.check_out_time;
                                setValueL(KEY_ATTENDANCE_OBJ, JSON.stringify(atndObj));
                            }
                        });
                    } else {
                        self.attandaceStatus(true);
                    }
                    myApp.hidePreloader();

                })
                .fail(function (err) {
                    myApp.hidePreloader();
                    myApp.alert(thrownError, AppMessages.COMN_ERR101);
                });
        }
    }, this);
    self.selectedItemClick = function (c, event) {
        if (c != null) {
            // myApp.showPreloader(AppMessages.COMN_INF102);
            var id = c.id;
            var name = c.name;
            var image = c.image;
            var reorder = c.reorder;
            var intQty = c.started_qty;
            var soldQty = c.end_qty;
            var selectdElement = event.currentTarget.id;
            setValueL(KEY_ITEMID, id);
            setValueL(KEY_ITEMNAME, name);
            setValueL(KEY_ITEMIMAGE, image);
            setValueL(KEY_ITEMREORDER, reorder);
            setValueL(KEY_ITMINTQTY, intQty);
            setValueL(KEY_ITMSOLDQTY, soldQty);
            setValueL(KEY_SELECTED_ITEM_ELEM, selectdElement);
            //redirect
            setTimeout(function () {
                navigateTo("items_sold.html");
            }, 500);
        } else {
            myApp.hidePreloader();
            myApp.alert(AppMessages.COMN_ERR100, AppMessages.COMN_ERR101);
        }
    }
    self.itemsSummary = function () {
        navigateTo('item_summary.html');
    }
    self.checkGPS = function () {
        //send checkin data
        myApp.confirm('Would you like to share your current location?', '', function () {
            myApp.showPreloader(AppMessages.COMN_INF102);
            checkGPSAvailability();
            sendLocation();
            myApp.hidePreloader();
            myApp.alert(AppMessages.COMN_INF108, AppMessages.COMN_INF105);
        }, function () {

        });
    }
}


/* ----------------------------------------------------------
 -                     Functions                          -
 ---------------------------------------------------------- */
function loadItems() {
    if (navFrom == "HOME") {
        getItems(
            function (cb) {
                var data = cb.data;
                if (cb.meta.status == 'true') {
                    if (data.length <= 0) {
                        myApp.addNotification({
                            title: 'We are sorry!',
                            message: 'You have no activations at the movement.'
                        });
                    } else {
                        setValueL(KEY_ITEMS_OBJ, JSON.stringify(data));
                        ko.utils.arrayForEach(data, function (item) {
                            koBinding.itemsData.push(item);
                        });
                    }
                }
                myApp.hideIndicator();
                myApp.hidePreloader();
            },
            function (err) {
                myApp.hideIndicator();
                myApp.hidePreloader();
                myApp.alert(err.responseText, AppMessages.COMN_ERR100);
            });
    } else {
        var itemsFromCache = JSON.parse(getValueL(KEY_ITEMS_OBJ));
        ko.utils.arrayForEach(itemsFromCache, function (item) {
            koBinding.itemsData.push(item);
        });
    }
    myApp.hideIndicator();
}

function getItems(cb, err) {
    var actID = getValueL(KEY_ACTID);
    var url = getValueL(KEY_APIBASEURL) + "v2/sales/items/" + actID;
    var type = "GET";
    var timeOut = 2000;
    if(isOnline()){
        AjaxCall(url, {}, timeOut, type, cb, err, true, true);
    }else{
        myApp.hidePreloader();
        myApp.alert(AppMessages.COMN_INF101, AppMessages.COMN_ERR100);
    }
}


function getActivationData(vm) {
    var actID = getValueL(KEY_ACTID);
    var actName = getValueL(KEY_ACTNAME);
    var actOutlet = getValueL(KEY_ACTOUTLET);
    var userId = getValueL(KEY_USRID);
    vm.activationID(actID);
    vm.activationName(actName);
    vm.activationOutlet(actOutlet);
    vm.userId(userId);
}

//Get the geo location via jQuery promise
var getLocation = function (callback, highAccur, timeout, maxAge) {
    //Deferred object for promise interface
    var deferred = new $.Deferred();

    if ($.isFunction(callback)) {
        deferred.then(callback);
    }

    navigator.geolocation.getCurrentPosition(
        function (position) {
            deferred.resolve(position);
        },
        function (err) {
            deferred.reject(err);
        },
        {enableHighAccuracy: highAccur}
    );

    //{ enableHighAccuracy: highAccur, timeout: timeout, maximumAge: maxAge }

    return deferred.promise();
};

//Process and send location data to the server
function processAndSendLocationData(position, actID, usrId, action, cb) {
    var result = null; // 0 - ideal , 1 - checked in, 2 checked - out
    var data = new LocationData();
    data.latitude = checkProperValue(position) ? checkAndGetValue(position.coords.latitude, 0) : null;
    data.longitude = checkProperValue(position) ? checkAndGetValue(position.coords.longitude, 0) : null;
    data.activation_id = actID;
    data.user_id = usrId;
    data.at_type = action;

    var url = getValueL(KEY_APIBASEURL) + "v2/attendance";
    var params = data;
    var type = "POST";
    var timeOut = 2000;

    if(isOnline()){
        AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
            if (data.meta.status == "true") {
                cb(data.data);
                // check out thing here
                myApp.hidePreloader();
                //myApp.alert(AppMessages.ITM_INF100,AppMessages.COMN_INF105);
            } else {
                myApp.hidePreloader();
                myApp.alert(thrownError, AppMessages.COMN_ERR101);
            }
        }, function (xhr, ajaxOptions, thrownError) {
            myApp.hidePreloader();
            myApp.alert(thrownError, AppMessages.COMN_ERR101);
        });
    }else{
        myApp.hidePreloader();
        myApp.alert(AppMessages.COMN_INF101, AppMessages.COMN_ERR100);
    }
}

//Check and choose the value to return, v: value to check, defV: Default value to return if check fails
function checkAndGetValue(v, defV) {
    return checkEmpty(v) ? v : defV;
}

//Location data sent to the server
var LocationData = function () {

    /* Mandatory */

    //Latitude
    this.latitude = null;
    //Longitude
    this.longitude = null;
    //Activation ID
    this.activation_id = null;
    //User ID
    this.userId = null;
    //Action (Checking || checkout)
    this.at_type = null;

    /* Optional */

}

function check_Attendance(vm) {
    var data = "";
    var atndFromCache = JSON.parse(getValueL(KEY_ATTENDANCE_OBJ));
    if (!isObjectEmpty(atndFromCache) && getValueL(KEY_NAVFROM) == "ITEM_SOLD") {
        myApp.hidePreloader();
        data = atndFromCache.data[0];
        if (data.check_in_time != "00:00:00" && data.check_out_time != "00:00:00") {
            $("#checkIn").prop("disabled", true);
            vm.attandaceTime(data.check_out_time);
            vm.attandaceLabel('CheckOut');
            vm.checkedIn(true);
            vm.checkedOut(true);
        } else if (data.check_in_time == "00:00:00") {
            myApp.confirm('Would you like to checking for today?', '', function () {
                vm.pageLoad(true);
                vm.attandaceStatus(true);
            }, function () {
                vm.checkedIn(false);
                vm.checkedOut(false);
            });
        } else {
            vm.attandaceTime(data.check_in_time);
            vm.attandaceLabel('CheckIn');
            vm.checkedIn(true);
            vm.checkedOut(false);
            //vm.attandaceStatus(true);
            $('#checkIn').attr('checked', true);
        }

    } else {
        getAttandace(function (cb) {
            myApp.hidePreloader();
            data = cb.data[0];
            setValueL(KEY_ATTENDANCE_OBJ, JSON.stringify(cb));
            if (data.check_in_time != "00:00:00" && data.check_out_time != "00:00:00") {
                $("#checkIn").prop("disabled", true);
                vm.attandaceTime(data.check_out_time);
                vm.attandaceLabel('CheckOut');
                vm.checkedIn(true);
                vm.checkedOut(true);
            } else if (data.check_in_time == "00:00:00") {
                myApp.confirm('Would you like to checking for today?', '', function () {
                    vm.pageLoad(true);
                    vm.attandaceStatus(true);
                }, function () {
                    vm.checkedIn(false);
                    vm.checkedOut(false);
                });
            } else {
                vm.attandaceTime(data.check_in_time);
                vm.attandaceLabel('CheckIn');
                vm.checkedIn(true);
                vm.checkedOut(false);
                //vm.attandaceStatus(true);
                $('#checkIn').attr('checked', true);
            }
        }, function (err) {
            myApp.alert(thrownError, AppMessages.COMN_ERR101);
        })
    }
}

//Get campaign location information
function getAttandace(cb, err) {
    var url = getValueL(KEY_APIBASEURL);
    var userID = getValueL(KEY_USRID);
    var actID = getValueL(KEY_ACTID);
    url = url + "v2/attendance?user_id=" + userID + "&activation_id=" + actID;
    var data = {};
    if(isOnline()){
        AjaxCall(url, data, 2000, "GET", cb, err, true, true, undefined, null);
    }else{
        myApp.hidePreloader();
        myApp.alert(AppMessages.COMN_INF101, AppMessages.COMN_ERR100);
    }
}
