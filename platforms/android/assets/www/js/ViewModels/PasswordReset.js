
/* ----------------------------------------------------------
   -                     Startup                            -
   ---------------------------------------------------------- */

$(document).ready(function () {
    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            koBinding = new ResetPasswordViewModel();
            ko.applyBindings(koBinding);
        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Device ready event subscribe
document.addEventListener("deviceready", function () {

    checkGPSAvailability(); // start the gps check

    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    navigator.app.backHistory()
}


/* ----------------------------------------------------------
   -                     View Models                        -
   ---------------------------------------------------------- */

function ResetPasswordViewModel() {
    var self = this;

    self.userName = ko.observable("");
    self.currentPassword = ko.observable("");
    self.newPassword = ko.observable("");

    self.hasUsernameFocus = ko.observable(false);
    self.hasCuurentPasswordFocus = ko.observable(false);
    self.hasNewPasswordFocus = ko.observable(false);

    self.enterKeyOnUsername = function (d, e) {
        if (e.keyCode == 13) {            
            self.hasCuurentPasswordFocus(true);
        }
        else {
            return true;
        }
    }
    self.enterKeyOnCurrentPassword = function (d, e) {
        if (e.keyCode == 13) {
            self.hasNewPasswordFocus(true);
        }
        else {
            return true;
        }
    }

    self.resetClick = function () {
        var val = validation(self.userName(), self.currentPassword(), self.newPassword());
        if (val) {
            myApp.showPreloader(AppMessages.USRREG_INF100);
            resetPassword(self.userName(), self.currentPassword(), self.newPassword());
        }


    }
}
/* ----------------------------------------------------------
   -                     Functions                          -
   ---------------------------------------------------------- */

function resetPassword(un, cp, np) {
    var url = getValueL(KEY_APIBASEURL) + "v2/auth/reset";
    var params = { "user_name": un, "current_password" :  cp, "new_password" : np };
    var type = "POST";
    var timeOut = 2000;
    if(isOnline()){
        AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
            if (data.meta.status == true || data.meta.status == "true") {
                myApp.hidePreloader();
                myApp.alert(AppMessages.LOGIN_VAL105, AppMessages.COMN_INF105);
                //mainView.router.load('index.html')
            } else if(data.meta.status == false || data.meta.status == "false"){
                myApp.hidePreloader();
                myApp.alert(data.error, AppMessages.USRREG_VAL104);
            }else{
                myApp.hidePreloader();
                myApp.alert(data.error, AppMessages.COMN_ERR100);
            }
        }, function (xhr, ajaxOptions, thrownError) {
            myApp.hidePreloader();
            myApp.alert(thrownError, AppMessages.COMN_ERR100);
        });
    }else{
        myApp.hidePreloader();
        myApp.alert(AppMessages.COMN_INF101, AppMessages.COMN_ERR100);
    }

}

function validation(un, cp, np) {
    var rv = true;

    if (!checkEmpty(un)) {
        myApp.alert(AppMessages.USRREG_VAL100, AppMessages.COMN_INF100);
        rv = false;
    } else if (!checkEmpty(cp)) {
        myApp.alert(AppMessages.USRREG_VAL101, AppMessages.COMN_INF100);
        rv = false;
    } else if (!checkEmpty(np)) {
        myApp.alert(AppMessages.USRREG_VAL102, AppMessages.COMN_INF100);
        rv = false;
    }else if ( cp == np){
        myApp.alert(AppMessages.USRREG_VAL103, AppMessages.COMN_INF100);
        rv = false;
    }
    return rv;
}

