

/* ----------------------------------------------------------
   -                    Object Handlers                     -
   ---------------------------------------------------------- */

function getProperValue(itemToCheck, defaultValue) {
    /// <summary>
    /// Check and get the value
    /// </summary>
    /// <param name="itemToCheck">Value to be checked</param>
    /// <param name="defaultValue">Default value to return if check fails</param>
    /// <returns type="">Checked or Default value</returns>
    if (itemToCheck != null && itemToCheck != undefined)
        return itemToCheck;
    else
        return defaultValue;
}

function checkFunction(fun) {
    /// <summary>
    /// Check if the given item is a function
    /// </summary>
    /// <param name="itemToCheck">Value to be checked</param>
    /// <returns type="">True if value is available else False</returns>

    if (checkEmpty(fun) && typeof fun == 'function') {
        return true;
    } else {
        return false;
    }

}


//Get user profile info
function getUserProfile(cb, err) {
    var url = getValueL(KEY_APIBASEURL);
    url = url + "user/profile_info/";
    AjaxCall(url, {}, 0, "GET", cb, err, true, true);
}

function checkProperValue(itemToCheck) {
    /// <summary>
    /// Check if the given item is undefined or null
    /// </summary>
    /// <param name="itemToCheck">Value to be checked</param>
    /// <returns type="">True if value is available else False</returns>
    if (itemToCheck != null && itemToCheck != undefined)
        return true;
    else
        return false;
}

function checkEmpty(itemToCheck) {
    /// <summary>
    /// Check if the given item is undefined or null or empty
    /// </summary>
    /// <param name="itemToCheck">Value to be checked</param>
    /// <returns type="">True if value is available else False</returns>

    if (checkProperValue(itemToCheck) && itemToCheck !== "") {
        return true;
    }
    else
        return false;
}

function isInt(value) {
    /// <summary>
    /// Check if the given item is integer
    /// </summary>
    /// <param name="value">Value to be checked</param>
    /// <returns type="">True if value is integer else False</returns>
    var x;
    if (isNaN(value)) {
        return false;
    }
    x = parseFloat(value);
    return (x | 0) === x;
}

/* ----------------------------------------------------------
   -                Utility Functions                       -
   ---------------------------------------------------------- */

function scrollToTop(element, speed) {
    var s = checkEmpty(speed) ? speed : "slow";
    var elem = checkEmpty(element) ? element : "html, body";
    $(elem).animate({ scrollTop: 0 }, s);
}

function AjaxCall(url, params, timeOut, type, ajxSuccessFn, ajxFailFn, processData, authenticate, additionalData, tag) {
    /// <summary>
    /// Send jquery ajax request
    /// </summary>
    /// <param name="url">URL to send the request to</param>
    /// <param name="params">Ajax body parameters</param>
    /// <param name="timeOut">The local timeout (in milliseconds) for the request. If null default value will be assigned</param>
    /// <param name="type">Specifies the type of request.</param>
    /// <param name="ajxSuccessFn">A function to be run when the request succeeds</param>
    /// <param name="ajxFailFn">A function to run if the request fails.</param>
    /// <param name="processData">A Boolean value specifying whether or not data sent with the request should be transformed into a query string. Default is true</param>
    /// <param name="authenticate">If true authentication data will be sent to the server with the request, Default is false</param>
    /// <param name="tag">Tagged data to be returned with success or failure</param>

    timeOut = checkEmpty(timeOut) ? 6000 : timeOut;
    type = type.toUpperCase() != "GET" &&
          type.toUpperCase() != "POST" &&
          type.toUpperCase() != "PUT" &&
          type.toUpperCase() != "DELETE" ? "POST" : type;
    authenticate = checkEmpty(authenticate) ? authenticate : false;

    processData = checkProperValue(processData) ? processData : true;

    var tagged = checkEmpty(tag) ? tag : null;

    // if (authenticate && checkProperValue(params)) {
    //     params.user_session = getValueL(KEY_USRSESSON);
    //     params.user_key = getValueL(KEY_USRKEY);
    // }

    // if (checkEmpty(additionalData)) {
    //     var finalData = $.param(params);
    //     finalData += ("&" + additionalData);
    //     params = finalData;
    // }
    $.ajax({
        type: type.toUpperCase(),
        cache: false,
        url: url,
        data: JSON.stringify(params),
        processData: processData,
        dataType: 'json',
        contentType: 'application/json',
        success: function (data, textStatus, jqXHR) {
            if (checkFunction(ajxSuccessFn)) {
                ajxSuccessFn(data, textStatus, jqXHR, tagged);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            if (checkEmpty(ajxFailFn)) {
                ajxFailFn(xhr, ajaxOptions, thrownError, tagged);
            }
        },
        timeout: timeOut
    }).complete(function (xhr, status) {

    });
}

function navigateTo(location) {
    /// <summary>
    /// Redirect in to another page.
    /// </summary>
    /// <param name="location">include destination url<br />
    /// file name or file name with directory structure<br />
    /// </param> 

    var url = location;
    if (checkEmpty(url)) {
        myApp.hidePreloader();
        setTimeout(function () {
            window.location = url;
        }, 300);
        navigator.splashscreen.hide();
    }
}

var isOnline = function () {
    /// <summary>
    /// check is connected to the internet ? . please install  
    ///https://github.com/apache/cordova-plugin-network-information.
    /// plug-in first before use this function
    /// </summary>
    /// <returns type="">Return true if connected</returns>

    var isConnected = false;
    //var networkConnection = navigator.connection;
    if (!navigator.connection.type) {
        return false;
    }
    switch (navigator.connection.type.toLowerCase()) {
        case 'ethernet':
        case 'wifi':
        case 'cell_2g':
        case 'cell_3g':
        case 'cell_4g':
        case '2g':
        case '3g':
        case '4g':
        case 'cell':
        case 'cellular':
            isConnected = true;
            break;
    }
    return isConnected;
}

function getConnectionType() {
    /// <summary>
    /// Get internet connection type. please install  
    ///https://github.com/apache/cordova-plugin-network-information.
    /// plug-in first before use this function
    /// </summary>
    /// <returns type="">Return formatted network type in lower case</returns>
    switch (navigator.connection.type.toLowerCase()) {
        case 'ethernet':
            return "ethernet";
            break;
        case 'wifi':
            return "wifi";
            break;
        case 'cell_2g':
            return "2g";
            break;
        case 'cell_3g':
            return "3g";
            break;
        case 'cell_4g':
            return "4g";
            break;
        case '2g':
            return "2g";
            break;
        case '3g':
            return "3g";
            break;
        case '4g':
            return "4g";
            break;
        case 'cell':
            return "cell";
            break;
        case 'cellular':
            return "cell"
            break;
    }
}

var GetPlatform = function () {
    /// <summary>
    /// Get platfoem of device      
    /// plug-in first before use this function(cordova plugin add org.apache.cordova.device);
    /// </summary>
    /// <returns type="">
    // Depending on the device, a few examples are:
    //   - "Android"
    //   - "BlackBerry 10"
    //   - "iOS"
    //   - "WinCE"
    //   - "Tizen"
    //</returns>

    return device.platform;
}

function randomString(length, characters) {
    /// <summary>
    /// Generate random string
    /// </summary>
    /// <param name="length">Length of the random string</param>
    /// <param name="characters">Characters to use in the string</param>
    /// <returns type="">Return generated string</returns>

    var chars = checkEmpty(characters) ?
        characters :
        "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;

}

function isLocationEnabled(){
    /// <summary>
    /// Check gps id enabled on device
    /// </summary>
    /// <returns type="">Return if enabled - true, if disabled - false, if error - error msg</returns>
    // cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
    //     return enabled ? "TRUE" : "FALSE";
    // }, function(error){
    //     console.error("The following error occurred: "+error);
    //     return "error";
    // });

    cordova.plugins.diagnostic.isLocationAuthorized(function(enabled){
        console.log("Location authorization is " + (enabled ? "enabled" : "disabled"));
    }, function(error){
        console.error("The following error occurred: "+error);
    });
}

function isValidDate(string){
    var d = new Date(string);
    var result = null;
    if ( Object.prototype.toString.call(d) === "[object Date]" ) {
        // it is a date
        if ( isNaN( d.getTime() ) ) {  // d.valueOf() could also work
            // date is not valid
            result = false;
        }
        else {
            // date is valid
            result = true;
        }
    }
    else {
        // not a date
        result = false;
    }
    return result;
}

function getFormatedTime(time){
    var hours =  time.split(/[ :]/)[0];
    var minutes = " : "+time.split(/[ :]/)[1];
    var suffix = hours >= 12 ? " PM":" AM";
    return time != "-:-" ? ((hours % 12) || 12) + minutes + suffix : "-:-";
}

//Check and choose the value to return, v: value to check, defV: Default value to return if check fails
function checkAndGetValue(v, defV) {
    return checkEmpty(v) ? v : defV;
}

/* ----------------------------------------------------------
 -                    GPS Related functions                     -
 ---------------------------------------------------------- */
function checkAuthorization(){
    cordova.plugins.diagnostic.isLocationAuthorized(function(authorized){
        console.log("Location is " + (authorized ? "authorized" : "unauthorized"));
        if(authorized){
            checkDeviceSetting();
        }else{
            cordova.plugins.diagnostic.requestLocationAuthorization(function(status){
                switch(status){
                    case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                        console.log("Permission granted");
                        checkDeviceSetting();
                        break;
                    case cordova.plugins.diagnostic.permissionStatus.DENIED:
                        console.log("Permission denied");
                        //change icon
                        $('#gps').removeClass('fa-map-marker');
                        $('#gps').addClass(' fa-ban');
                        myApp.hidePreloader();
                        // User denied permission
                        break;
                    case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                        console.log("Permission permanently denied");
                        //change icon
                        $('#gps').removeClass('fa-map-marker');
                        $('#gps').addClass(' fa-ban');
                        myApp.hidePreloader();
                        // User denied permission permanently
                        break;
                }
            }, function(error){
                console.error(error);
            });
        }
    }, function(error){
        console.error("The following error occurred: "+error);
        myApp.hidePreloader();
    });
}

function checkDeviceSetting(){
    cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
        console.log("GPS location setting is " + (enabled ? "enabled" : "disabled"));
        if(!enabled){
            cordova.plugins.locationAccuracy.request(function (success){
                console.log("Successfully requested high accuracy location mode: "+success.message);
                //change icon
                $('#gps').removeClass('fa-ban');
                $('#gps').addClass('fa-map-marker');
                myApp.hidePreloader();
            }, function onRequestFailure(error){
                console.error("Accuracy request failed: error code="+error.code+"; error message="+error.message);
                if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
                    if(confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")){
                        cordova.plugins.diagnostic.switchToLocationSettings();
                        $('#gps').removeClass('fa-ban');
                        $('#gps').addClass('fa-map-marker');
                        myApp.hidePreloader();
                    }
                }
            }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
        }else{
            $('#gps').removeClass('fa-ban');
            $('#gps').addClass('fa-map-marker');
            myApp.hidePreloader();
        }
    }, function(error){
        console.error("The following error occurred: "+error);
    });
}

function checkGPSAvailability() {
    cordova.plugins.diagnostic.isGpsLocationAvailable(function (available) {
        console.log("GPS location is " + (available ? "available" : "not available"));
        if (!available) {
            checkAuthorization();
        } else {
            console.log("GPS location is ready to use");
            myApp.hidePreloader();
            $('#gps').removeClass('fa-ban');
            $('#gps').addClass('fa-map-marker');
        }
    }, function (error) {
        console.error("The following error occurred: " + error);
        myApp.hidePreloader();
    });
}

function tracking(){
    // setInterval(checkAuthorization, 1000 * 60 * 10);
    // setInterval(sendLocation, 1000 * 60);
    setInterval(sendLocation, 1000 * 60 * 30);
}

function sendLocation(){
    getLocationCommon(null, 'false', null, null)
        .then(function (position) {
            if(checkProperValue(position)){
                sendTrakingAjax(position);
            }
        })
        .fail(function (err) {
            checkAuthorization();
            console.log(err);
        });
}

//Get the geo location via jQuery promise
var getLocationCommon = function (callback, highAccur, timeout, maxAge) {
    //Deferred object for promise interface
    var deferred = new $.Deferred();

    if ($.isFunction(callback)) {
        deferred.then(callback);
    }

    navigator.geolocation.getCurrentPosition(
        function (position) {
            deferred.resolve(position);
        },
        function (err) {
            deferred.reject(err);
        },
        { enableHighAccuracy: highAccur }
    );

    //{ enableHighAccuracy: highAccur, timeout: timeout, maximumAge: maxAge }

    return deferred.promise();
};

function sendTrakingAjax(position){
    var latitude = checkProperValue(position) ? checkAndGetValue(position.coords.latitude, 0) : null;
    var longitude = checkProperValue(position) ? checkAndGetValue(position.coords.longitude, 0) : null;
    var userID = getValueL(KEY_USRID);
    var actID = getValueL(KEY_ACTID);

    var url = getValueL(KEY_APIBASEURL) + "v2/tracking";
    var params = {"activation_id": actID, "user_id": userID, "longitude": longitude, latitude: latitude};
    var type = "POST";
    var timeOut = 2000;
    AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        if(data.meta.status == "true"){
            console.log("successfully send tracking data");
            myApp.hidePreloader();
        }else{
            console.log("failed to send tracking data");
            myApp.hidePreloader();
        }
    }, function (xhr, ajaxOptions, thrownError) {
        console.log(thrownError);
        myApp.hidePreloader();
    });
}

/* ----------------------------------------------------------
 -                    Application common functions                   -
 ---------------------------------------------------------- */

function logOut() {
    myApp.confirm('Are you sure?','' ,function () {
        localStorage.clear();
        navigateTo('index.html');
    });
}