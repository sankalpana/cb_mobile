/* ----------------------------------------------------------
   -                  Global Variables                      -
   ---------------------------------------------------------- */

var koBinding;

/* ----------------------------------------------------------
   -              Knockout Custom Bindings                  -
   ---------------------------------------------------------- */

/* ----------------------------------------------------------
   -                     Startup                            -
   ---------------------------------------------------------- */

$(document).ready(function () {
    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
     function (r) {
         console.log("localization loaded: " + r);
         applyKoBindings();
         // myApp.showPreloader(AppMessages.COMN_INF103); 

     },
     function (err) {
         console.error("Failed to load locale: " + err);
     });
});

//Device ready event subscribe
document.addEventListener("deviceready", function () {
    tracking();
    checkGPSAvailability(); // start the gps check

    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous  page
    navigator.app.backHistory()
}


//Knockout binging initialization
function applyKoBindings() {
    koBinding = new LeaveViewModel();
    setUserData(koBinding);
    loadDropdowns(koBinding);
    loadLeaveData(function (cb) {
        var data = cb.data;
        if (data.length <= 0) {
            myApp.addNotification({
                title: 'We are sorry!',
                message: 'You have no leaves available at the movement.'
            });
        } else {
            ko.utils.arrayForEach(data, function (item) {
                koBinding.leaveData.push(item);
            });
        }
    }, function (err) {

    }, function (tag) {

    });
    ko.applyBindings(koBinding);
}
/* ----------------------------------------------------------
   -                     View Models                        -
   ---------------------------------------------------------- */

function LeaveViewModel() {
    var self = this;
    self.userName = ko.observable();
    self.imageUrl = ko.observable();
    self.userId   = ko.observable();
    self.fromDate   = ko.observable();
    self.toDate   = ko.observable();
    self.reasons = ko.observableArray([]);
    self.approver = ko.observableArray([]);
    self.selectedReasons = ko.observable(self.reasons()[0]);
    self.selectedApprover = ko.observable(self.approver()[0]);
    self.submitBtnClick = function () {
        var val = validation(self.fromDate(), self.toDate());
        if (val) {
            myApp.showPreloader(AppMessages.COMN_INF102);
            submitRequest(self.userId(), self.fromDate(), self.toDate(), self.selectedApprover(), self.selectedReasons());
        }
    }
    self.checkGPS = function () {
        //send checkin data
        myApp.confirm('Would you like to share your current location?', '', function () {
            myApp.showPreloader(AppMessages.COMN_INF102);
            checkGPSAvailability();
            sendLocation();
            myApp.hidePreloader();
            myApp.alert(AppMessages.COMN_INF108, AppMessages.COMN_INF105);
        }, function () {

        });
    }
}

/* ----------------------------------------------------------
   -                     Functions                          -
   ---------------------------------------------------------- */

function setUserData(koBinding){
    var user_name = getValueL(KEY_USERNAME);
    var user_image = getValueL(KEY_USERIMAGE);
    var user_id = getValueL(KEY_USRID);
    koBinding.imageUrl(user_image);
    koBinding.userName(user_name);
    koBinding.userId(user_id);
}

//Get leave information
function loadLeaveData(cb, err, tag) {
    var url = getValueL(KEY_APIBASEURL);
    var userID = getValueL(KEY_USRID);
    //url = url + "v2/activation/promoter/"+userID;
    url = url + "v2/leave";
    var data = {};
    AjaxCall(url, data, 2000, "GET", cb, err, true, true, undefined, tag);
}

function loadDropdowns(koBinding) {
    getDropdowns(function(cb) {
        var data = cb.data;
        if(data.length <= 0){
            myApp.hidePreloader();
            myApp.alert(AppMessages.LEAVE_INF100, AppMessages.COMN_INF107);
        }else{
            ko.utils.arrayForEach(data[0].reasons, function (item) {
                koBinding.reasons.push(item);
            });
            ko.utils.arrayForEach(data[0].approved_by, function (item) {
                koBinding.approver.push(item);
            });
            myApp.hidePreloader();
        }
    },function (err){
        myApp.hideIndicator();
        myApp.hidePreloader();
        myApp.alert(err.responseText, AppMessages.COMN_ERR100);
    });

}

//Get leave information
function getDropdowns(cb, err) {
    var url = getValueL(KEY_APIBASEURL) + "v2/leave/combo";
    var data = {};
    AjaxCall(url, data, 2000, "GET", cb, err, true, true, undefined, null);
}

function validation(fromDate, toDate) {
    var rv = true;

    if (!isValidDate(fromDate)) {
        myApp.alert(AppMessages.LEAVE_ERR100, AppMessages.COMN_INF100);
        rv = false;
    } else if (!isValidDate(toDate)) {
        myApp.alert(AppMessages.LEAVE_ERR101, AppMessages.COMN_INF100);
        rv = false;
    }
    return rv;
}

function submitRequest(userId, fDate, tDate, approver, reason ) {
    var url = getValueL(KEY_APIBASEURL) + "v2/leave";
    var params = { "user_id": userId, "from_date":  fDate, "to_date": tDate, "confirmed_by": approver, "reason": reason };
    var type = "POST";
    var timeOut = 2000;
    var res = AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        if (data.meta.status == true || data.meta.status == "true") {
            myApp.hidePreloader();
            myApp.alert(AppMessages.LEAVE_INF100, AppMessages.COMN_INF105);
            navigateTo('leave_list.html');
        } else if(data.meta.status == false || data.meta.status == "false"){
            myApp.hidePreloader();
            myApp.alert(AppMessages.LOGIN_VAL103, AppMessages.COMN_ERR100);
        }else{
            myApp.hidePreloader();
            myApp.alert(data.error, AppMessages.COMN_ERR100);
        }
    }, function (xhr, ajaxOptions, thrownError) {
        myApp.hidePreloader();
        myApp.alert(thrownError, AppMessages.COMN_ERR100);
    });
}