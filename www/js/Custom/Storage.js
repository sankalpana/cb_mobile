//Key for cached WebService API base url
var KEY_APIBASEURL = "APP_APIBASEURL";
//Key for image upload url
var KEY_APIIMAGEUPLOADURL = "APP_IMGUPURL";
//User session key
var KEY_USRSESSON = "USR_SESSON";
//User key
var KEY_USRKEY = "USR_KEY";
//Remember me flag
var KEY_RMBR = "USR_RMBR";
//User name
var KEY_USERNAME = "USR_USERNAME";
//User image
var KEY_USERIMAGE = "USR_IMAGE";
//User id
var KEY_USRID = "USR_ID";
//var user id
var KEY_USROBJ = "USR_OBJ";
//User type
var KEY_USRTYPE = "USR_TYPE";
//Activation id
var KEY_ACTID = "ACT_ID";
//Activation name
var KEY_ACTNAME = "ACT_NAME";
//Activation outlet
var KEY_ACTOUTLET = "ACT_OUTLET";
//Selected item id
var KEY_ITEMID = "ITEM_ID";
//Selected item name
var KEY_ITEMNAME = "ITEM_NAME";
//Selected item image
var KEY_ITEMIMAGE = "ITEM_IMAGE";
//Reorder status
var KEY_ITEMREORDER = "ITEM_REODER";
//Initial quantity
var KEY_ITMINTQTY = "ITEM_INIT_QTY";
//Sold quentity
var KEY_ITMSOLDQTY = "ITEM_SOLD_QTY";
//Navigated from page.
var KEY_NAVFROM = "NAV_FROM";
//Items object
var KEY_ITEMS_OBJ = "ITEMS_OBJ";
//Promoter name
var KEY_PROMOTER = "ACT_PROMOTER";
//Task object
var KEY_TSKOBJ = "TSK_OBJ";
//Image upload retry count
var KEY_RETRY_COUNT = "RETRY_COUNT";
//attendance obj
var KEY_ATTENDANCE_OBJ = "ATTENDANCE_OBJ";

var KEY_SELECTED_ITEM_ELEM = "SELECTED_ITEM_ELEM";
//this holds app version
var KEY_VERSION = "VERSION";

function getValueL(key) {
	return window.localStorage.getItem(key);
}
function setValueL(key, value) {
	window.localStorage.setItem(key, value);
}

function getValueS(key) {
	return window.sessionStorage.getItem(key);
}
function setValueS(key, value) {
	window.sessionStorage.setItem(key, value);
}
