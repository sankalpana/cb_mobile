/* ----------------------------------------------------------
 -                  Global Variables                      -
 ---------------------------------------------------------- */

//Knockout view model variable
var koBinding;
/* ----------------------------------------------------------
 -              Knockout Custom Bindings                  -
 ---------------------------------------------------------- */

//Knockout binding for Framework7 Radios/Checkboxes
ko.bindingHandlers.f7Chk = {
    init: function (element, valueAccessor) {
        $(element).on('change', function () {
            var observable = valueAccessor();
            //if (observable() !== this.checked)
            observable(this.checked);
        });
    },
    update: function (element, valueAccessor) {
        var observable = valueAccessor();
        //if ($(element).prop("checked") !== observable())
        $(element).prop("checked", observable());
    }
};

/* ----------------------------------------------------------
 -                      Events                            -
 ---------------------------------------------------------- */

//Device ready event subscribe
document.addEventListener("deviceready", function () {

    checkGPSAvailability(); // start the gps check
    tracking();
    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    //navigator.app.backHistory()
}

/* ----------------------------------------------------------
 -                     Startup                            -
 ---------------------------------------------------------- */

//Startup
$(document).ready(function () {
    //TODO: Temp remove this
    //setValueL(KEY_APIBASEURL, "https://api.instabuggy.com/v1.0/");
    //--

    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            checkUpdates();
            applyKoBindings();
        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Knockout binging initialization
function applyKoBindings() {
    koBinding = new HomeViewModel();
    setUserData(koBinding);
    myApp.showIndicator();
    loadActivationData(function (cb) {
        var data = cb.data;
        if (data.length <= 0) {
            myApp.hideIndicator();
            myApp.addNotification({
                title: 'We are sorry!',
                message: 'You have no activations at the movement.'
            });
        } else {
            ko.utils.arrayForEach(data, function (item) {
                koBinding.activationData.push(item);
            });
            myApp.hideIndicator();
        }
    }, function (err) {
        myApp.hideIndicator();
    }, function (tag) {
        myApp.hideIndicator();
    });
    ko.applyBindings(koBinding);
}

/* ----------------------------------------------------------
 -                     View Models                        -
 ---------------------------------------------------------- */

//Main ViewModel
function HomeViewModel() {
    var self = this;
    self.userName = ko.observable();
    self.imageUrl = ko.observable();
    self.userId = ko.observable();
    self.userType = ko.observable();
    self.displayUserType = ko.pureComputed(function () {
            return self.userType() == "0" ? "Sales Promoter" : "Sales Supervisor";
        }
    );
    self.activationData = ko.observableArray([]);
    self.itemsLinkClick = function (c, event) {
        if (c != null) {
            myApp.showPreloader(AppMessages.COMN_INF102);
            var actId = c.activation_id;
            var actName = c.activation_name;
            var actOutlet = c.outlet;
            setValueL(KEY_ACTID, actId);
            setValueL(KEY_ACTNAME, actName);
            setValueL(KEY_ACTOUTLET, actOutlet);
            setValueL(KEY_NAVFROM, "HOME");
            //Successful login
            setTimeout(function () {
                navigateTo("items.html");
            }, 200);
        } else {
            myApp.hidePreloader();
            myApp.alert(AppMessages.COMN_ERR100, AppMessages.COMN_ERR101);
        }
    }
    self.checkGPS = function () {
        //send checkin data
        myApp.confirm('Would you like to share your current location?', '', function () {
            myApp.showPreloader(AppMessages.COMN_INF102);
            checkGPSAvailability();
            sendLocation();
            myApp.hidePreloader();
            myApp.alert(AppMessages.COMN_INF108, AppMessages.COMN_INF105);
        }, function () {

        });
    }
}


/* ----------------------------------------------------------
 -                     Functions                          -
 ---------------------------------------------------------- */

function setUserData(koBinding) {
    var user_name = getValueL(KEY_USERNAME);
    var user_image = getValueL(KEY_USERIMAGE);
    var user_id = getValueL(KEY_USRID);
    var user_type = getValueL(KEY_USRTYPE);
    koBinding.imageUrl(user_image);
    koBinding.userName(user_name);
    koBinding.userId(user_id);
    koBinding.userType(user_type);
}

//Get campaign location information
function loadActivationData(cb, err, tag) {
    var url = getValueL(KEY_APIBASEURL);
    var userID = getValueL(KEY_USRID);
    url = url + "v2/activation/promoter/" + userID;
    var data = {};
    AjaxCall(url, data, 2000, "GET", cb, err, true, true, undefined, tag);

}

function checkGPSAvailability() {
    cordova.plugins.diagnostic.isGpsLocationAvailable(function (available) {
        console.log("GPS location is " + (available ? "available" : "not available"));
        if (!available) {
            checkAuthorization();
        } else {
            console.log("GPS location is ready to use");
            // document.getElementById("#gps").classList.remove('fa-map-marker');
            $('#gps').removeClass('fa-ban');
            $('#gps').addClass('fa-map-marker');
        }
    }, function (error) {
        console.error("The following error occurred: " + error);
    });
}





