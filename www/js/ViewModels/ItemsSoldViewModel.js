/* ----------------------------------------------------------
 -                  Global Variables                      -
 ---------------------------------------------------------- */

var koBinding;

/* ----------------------------------------------------------
 -              Knockout Custom Bindings                  -
 ---------------------------------------------------------- */

/* ----------------------------------------------------------
 -                     Startup                            -
 ---------------------------------------------------------- */

$(document).ready(function () {
    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            applyKoBindings();
            // myApp.showPreloader(AppMessages.COMN_INF103);

        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Device ready event subscribe
document.addEventListener("deviceready", function () {
    checkGPSAvailability();
    tracking();
    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    setValueL(KEY_NAVFROM, "ITEM_SOLD");
    //override the device back button to navigate to previous inline page
    navigateTo('items.html');
}

function applyKoBindings() {
    koBinding = new ItemsSoldViewModel();
    setItemData(koBinding);
    ko.applyBindings(koBinding);
}

ko.bindingHandlers.f7Chk = {
    init: function (element, valueAccessor) {
        $(element).on('change', function () {
            var observable = valueAccessor();
            observable(this.checked);
        });
    },
    update: function (element, valueAccessor) {
        var observable = valueAccessor();
        if (observable !== undefined) {
            $(element).prop("checked", observable());
            // console.log("has active " + selectedExistingFence().hasActiveDateRange);
        }

    }
};
/* ----------------------------------------------------------
 -                     View Models                        -
 ---------------------------------------------------------- */

function ItemsSoldViewModel() {
    var self = this;
    self.usrId = ko.observable();
    self.activationId = ko.observable();
    self.activationName = ko.observable();
    self.activationOutlet = ko.observable();
    self.id = ko.observable();
    self.name = ko.observable();
    self.itemImage = ko.observable();
    self.initialQty = ko.observable();
    self.soldQty = ko.observable();
    self.reOrderStatus = ko.observable();
    self.reOrder = ko.observable();
    self.reOrder.subscribe(function (checkedVal) {
        var action = checkedVal ? 1 : 0;
        if (action == 1) {
            myApp.confirm('Would you like to reorder this item?', '', function () {
                myApp.showPreloader(AppMessages.COMN_INF102);
                updateReorder(self.usrId(), self.activationId(), self.id(), action);
                updateReorderCache(self.id(), action);
            }, function () {
                $('#reorder').attr('checked', false);
            });
        } else {
            myApp.confirm('Do you want to cancel re-oder request?', '', function () {
                myApp.showPreloader(AppMessages.COMN_INF102);
                updateReorder(self.usrId(), self.activationId(), self.id(), action);
                updateReorderCache(self.id(), action);
            }, function () {
                $('#reorder').attr('checked', true);
            });
        }

    }, this);
    self.clickUpdate = function () {
        var val = validation(self.initialQty(), self.soldQty());
        if (val) {
            myApp.showPreloader(AppMessages.ITM_INF101);
            var lat = null;
            var long = null;
            getLocation(null, 'true', null, null)
                .then(function (position) {
                    lat = checkProperValue(position) ? checkAndGetValue(position.coords.latitude, 0) : 0;
                    long = checkProperValue(position) ? checkAndGetValue(position.coords.longitude, 0) : 0;
                })
                .fail(function (err) {

                });

            updateSales(self.usrId(), self.activationId(), self.id(), self.initialQty(), self.soldQty(), lat, long);
            //Update local items object
            var itemsObj = JSON.parse(getValueL(KEY_ITEMS_OBJ));
            var soldItem = {
                "id": self.id(),
                "name": self.name(),
                "started_qty": self.initialQty(),
                "end_qty": self.soldQty(),
                "reorder": self.reOrderStatus(),
                "image": self.itemImage()
            };
            var modifiedObj = findAndReplace(itemsObj, soldItem);
            setValueL(KEY_ITEMS_OBJ, JSON.stringify(modifiedObj));
            // itemsObj.push(soldItem);

        }
    }
    self.checkGPS = function () {
        //send checkin data
        myApp.confirm('Would you like to share your current location?', '', function () {
            myApp.showPreloader(AppMessages.COMN_INF102);
            checkGPSAvailability();
            sendLocation();
            myApp.hidePreloader();
            myApp.alert(AppMessages.COMN_INF108, AppMessages.COMN_INF105);
        }, function () {

        });
    }
}

/* ----------------------------------------------------------
 -                     Functions                          -
 ---------------------------------------------------------- */

function setItemData(koBinding) {
    var uid = getValueL(KEY_USRID);
    var actId = getValueL(KEY_ACTID);
    var actName = getValueL(KEY_ACTNAME);
    var actOutlet = getValueL(KEY_ACTOUTLET);
    var id = getValueL(KEY_ITEMID);
    var name = getValueL(KEY_ITEMNAME);
    var image = getValueL(KEY_ITEMIMAGE);
    var reorder = getValueL(KEY_ITEMREORDER);
    var intQty = getValueL(KEY_ITMINTQTY);
    var soldQty = getValueL(KEY_ITMSOLDQTY);

    koBinding.usrId(uid);
    koBinding.activationId(actId);
    koBinding.activationName(actName);
    koBinding.activationOutlet(actOutlet);
    koBinding.id(id);
    koBinding.name(name);
    koBinding.itemImage(image.toString());
    koBinding.initialQty(intQty);
    koBinding.soldQty(soldQty);

    if (reorder == 1) {
        koBinding.reOrderStatus(true);
        $('#reorder').attr('checked', true);
        // myApp.confirm('Would you like to reorder this item ?', '', function () {
        //     myApp.showPreloader(AppMessages.COMN_INF102);
        //     updateReorder(uid, actId, id, 1);
        //     $('#reorder').attr('checked', true);
        // }, function () {
        //
        // });
    } else {
        koBinding.reOrderStatus(false);
    }

}

function validation(iq, sq) {
    var rv = true;

    if (!checkEmpty(iq)) {
        myApp.alert(AppMessages.ITM_ERR100, AppMessages.COMN_INF100);
        rv = false;
    } else if (!checkEmpty(sq)) {
        myApp.alert(AppMessages.ITM_ERR101, AppMessages.COMN_INF100);
        rv = false;
    } else if (!checkProperValue(iq.match(/^\d+$/)) || !checkProperValue(sq.match(/^\d+$/))) {
        myApp.alert(AppMessages.ITM_ERR104, AppMessages.COMN_INF100);
        rv = false;
    }
    return rv;
}

function updateSales(userID, actvId, ItemId, initQty, saleQty, lat, long) {
    var url = getValueL(KEY_APIBASEURL) + "v2/sales";
    var params = {
        "user_id": userID,
        "activation_id": actvId,
        "item_id": ItemId,
        "started_qty": initQty,
        "end_qty": saleQty,
        "latitude": lat,
        "longitude": long
    };
    var type = "POST";
    var timeOut = 2000;
    AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        if (data.meta.status == true || data.meta.status == "true") {
            myApp.hidePreloader();
            myApp.alert(AppMessages.ITM_INF102, AppMessages.COMN_INF105);
        } else if (data.meta.status == false || data.meta.status == "false") {
            myApp.hidePreloader();
            myApp.alert(data.error, AppMessages.USRREG_VAL104);
        } else {
            myApp.hidePreloader();
            myApp.alert(data.error, AppMessages.COMN_ERR100);
        }
    }, function (xhr, ajaxOptions, thrownError) {
        myApp.hidePreloader();
        myApp.alert(thrownError, AppMessages.COMN_ERR100);
    });
}

//Check and choose the value to return, v: value to check, defV: Default value to return if check fails
function checkAndGetValue(v, defV) {
    return checkEmpty(v) ? v : defV;
}

//Get the geo location via jQuery promise
var getLocation = function (callback, highAccur, timeout, maxAge) {
    //Deferred object for promise interface
    var deferred = new $.Deferred();

    if ($.isFunction(callback)) {
        deferred.then(callback);
    }

    navigator.geolocation.getCurrentPosition(
        function (position) {
            deferred.resolve(position);
        },
        function (err) {
            deferred.reject(err);
        },
        {enableHighAccuracy: highAccur}
    );

    //{ enableHighAccuracy: highAccur, timeout: timeout, maximumAge: maxAge }

    return deferred.promise();
};

function updateReorder(userId, actvId, itemId, action) {
    var url = getValueL(KEY_APIBASEURL) + "v2/sales/reorder";
    var params = {"user_id": userId, "activation_id": actvId, "item_id": itemId, "at_type": action};
    var type = "POST";
    var timeOut = 2000;
    AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        if (data.meta.status == true || data.meta.status == "true") {
            myApp.hidePreloader();
            myApp.alert(AppMessages.ITM_INF103, AppMessages.COMN_INF105);
        } else if (data.meta.status == false || data.meta.status == "false") {
            myApp.hidePreloader();
            myApp.alert(data.error, AppMessages.COMN_ERR101);
        } else {
            myApp.hidePreloader();
            myApp.alert(data.error, AppMessages.COMN_ERR101);
        }
    }, function (xhr, ajaxOptions, thrownError) {
        myApp.hidePreloader();
        myApp.alert(thrownError, AppMessages.COMN_ERR100);
    });
}

function findAndReplace(obj, replaceObj) {
    var object = obj;
    for (var x in object) {
        if (typeof object[x] == typeof {}) {
            findAndReplace(object[x], replaceObj);
        }
        if (object[x] == replaceObj.id) {
            object['started_qty'] = replaceObj.started_qty;
            object['end_qty'] = replaceObj.end_qty;
            object['reorder'] = replaceObj.reorder;
            break; // uncomment to stop after first replacement
        }
    }

    return object;
}

function findAndReplaceReorder(obj, replaceObj) {
    var object = obj;
    for (var x in object) {
        if (typeof object[x] == typeof {}) {
            findAndReplaceReorder(object[x], replaceObj);
        }
        if (object[x] == replaceObj.id) {
            object['reorder'] = replaceObj.reorder;
            break; // uncomment to stop after first replacement
        }
    }

    return object;
}

function updateReorderCache(itemId, action) {
    //Update local items object
    var itemsObj = JSON.parse(getValueL(KEY_ITEMS_OBJ));
    var reorderItem = {
        "id": itemId,
        "reorder": action
    };
    var modifiedObj = findAndReplaceReorder(itemsObj, reorderItem);
    setValueL(KEY_ITEMS_OBJ, JSON.stringify(modifiedObj));
}

// // Listen for input event on numInput.
// $(':num').on('input', function(){
//     // Let's match only digits.
//     var num = this.value.match(/^\d+$/);
//     if (num === null) {
//         // If we have no match, value will be empty.
//         this.value = "";
//     }
// }, false)