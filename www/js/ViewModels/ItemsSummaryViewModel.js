/* ----------------------------------------------------------
 -                  Global Variables                      -
 ---------------------------------------------------------- */

var koBinding;

/* ----------------------------------------------------------
 -              Knockout Custom Bindings                  -
 ---------------------------------------------------------- */

/* ----------------------------------------------------------
 -                     Startup                            -
 ---------------------------------------------------------- */

$(document).ready(function () {
    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            applyKoBindings();
            // myApp.showPreloader(AppMessages.COMN_INF103);

        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Device ready event subscribe
document.addEventListener("deviceready", function () {
    tracking();
    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    navigator.app.backHistory()
}

function applyKoBindings() {
    myApp.showPreloader(AppMessages.COMN_INF102);
    koBinding = new ItemsSummaryViewModel();
    setSummeryData(koBinding);
    loadSummary(koBinding);
    ko.applyBindings(koBinding);
}

/* ----------------------------------------------------------
 -                     View Models                        -
 ---------------------------------------------------------- */

function ItemsSummaryViewModel() {
    var self = this;
    self.usrId = ko.observable();
    self.activationId = ko.observable();
    self.activationName = ko.observable();
    self.activationOutlet = ko.observable();
    self.totalFootFall = ko.observable();
    self.conversionCount = ko.observable();
    self.salesDate = ko.observable();
    self.totalSale = ko.observable();
    self.itemCount = ko.observable();
    self.initFootFall = ko.observable();
    self.initConversionCount = ko.observable();
    self.remarks = ko.observable();
    self.salesData = ko.observableArray([]);
    self.confirmStatus = ko.observable();
    self.summaryObj = ko.observable();
    self.clickConfirm = function () {
        var val = validation(self.totalFootFall(), self.conversionCount());
        if (val) {
            myApp.showPreloader(AppMessages.ITM_INF101);
            var obj = self.summaryObj();
            obj.data[0].foot_fall = self.totalFootFall();
            obj.data[0].conversion_count = self.conversionCount();
            obj.data[0].remarks = self.remarks();
            updateItemSummary(obj);
        }
    }
    self.navigatToTarget = function () {
        navigateTo('traget.html');
    }
}

/* ----------------------------------------------------------
 -                     Functions                          -
 ---------------------------------------------------------- */

function setSummeryData(koBinding) {
    var uid = getValueL(KEY_USRID);
    var actId = getValueL(KEY_ACTID);
    var actName = getValueL(KEY_ACTNAME);
    var actOutlet = getValueL(KEY_ACTOUTLET);
    koBinding.usrId(uid);
    koBinding.activationId(actId);
    koBinding.activationName(actName);
    koBinding.activationOutlet(actOutlet);
}

function validation(totalFF, conCount) {
    var rv = true;

    if (!checkEmpty(totalFF)) {
        myApp.alert(AppMessages.ITM_ERR102, AppMessages.COMN_INF100);
        rv = false;
    } else if (!checkEmpty(conCount)) {
        myApp.alert(AppMessages.ITM_ERR103, AppMessages.COMN_INF100);
        rv = false;
    }
    return rv;
}

function updateItemSummary(obj) {
    var url = getValueL(KEY_APIBASEURL) + "v2/sales/summary";
    var params = obj;
    var type = "POST";
    var timeOut = 2000;
    var res = AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        if (data.meta.status == true || data.meta.status == "true") {
            myApp.hidePreloader();
            myApp.alert(AppMessages.ITM_INF105, AppMessages.COMN_INF105);
        } else if (data.meta.status == false || data.meta.status == "false") {
            myApp.hidePreloader();
            myApp.alert(data.error, AppMessages.COMN_ERR101);
        } else {
            myApp.hidePreloader();
            myApp.alert(data.error, AppMessages.COMN_ERR100);
        }
    }, function (xhr, ajaxOptions, thrownError) {
        myApp.hidePreloader();
        myApp.alert(thrownError, AppMessages.COMN_ERR100);
    });
}

//Check and choose the value to return, v: value to check, defV: Default value to return if check fails
function checkAndGetValue(v, defV) {
    return checkEmpty(v) ? v : defV;
}

//Get the geo location via jQuery promise
var getLocation = function (callback, highAccur, timeout, maxAge) {
    //Deferred object for promise interface
    var deferred = new $.Deferred();

    if ($.isFunction(callback)) {
        deferred.then(callback);
    }

    navigator.geolocation.getCurrentPosition(
        function (position) {
            deferred.resolve(position);
        },
        function (err) {
            deferred.reject(err);
        },
        {enableHighAccuracy: highAccur}
    );

    //{ enableHighAccuracy: highAccur, timeout: timeout, maximumAge: maxAge }

    return deferred.promise();
};


function loadSummary(koBinding) {
    myApp.showIndicator();
    getSummary(
        function (cb) {
            if (cb.meta.status == 'true') {
                var data = cb.data;
                if (data.length <= 0) {
                    myApp.hideIndicator();
                    myApp.hidePreloader();
                    myApp.alert(AppMessages.SALE_INF100, AppMessages.COMN_INF107);
                } else {
                    koBinding.summaryObj(cb);
                    koBinding.totalSale(data[0].total_sale);
                    koBinding.itemCount(data[0].item_count);
                    koBinding.totalFootFall(data[0].foot_fall);
                    koBinding.conversionCount(data[0].conversion_count);
                    koBinding.confirmStatus(data[0].confirm_status);
                    koBinding.salesDate(data[0].sales_date);
                    ko.utils.arrayForEach(data[0].sales, function (item) {
                        koBinding.salesData.push(item);
                    });
                    if (data[0].confirm_status == 1) {
                        $(".summary-confirm").attr("disabled", true);
                    }
                    myApp.hidePreloader();
                    myApp.hideIndicator();
                }
            }
            myApp.hideIndicator();
            myApp.hidePreloader();
        },
        function (err) {
            myApp.hideIndicator();
            myApp.hidePreloader();
            myApp.alert(err.responseText, AppMessages.COMN_ERR100);
        });
}

function getSummary(cb, err) {
    var actID = getValueL(KEY_ACTID);
    var url = getValueL(KEY_APIBASEURL) + "v2/sales/summary/" + actID;
    var type = "GET";
    var timeOut = 2000;
    AjaxCall(url, {}, timeOut, type, cb, err, true, true);

}