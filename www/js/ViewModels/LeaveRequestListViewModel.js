/* ----------------------------------------------------------
 -                  Global Variables                      -
 ---------------------------------------------------------- */

var koBinding;

/* ----------------------------------------------------------
 -              Knockout Custom Bindings                  -
 ---------------------------------------------------------- */

/* ----------------------------------------------------------
 -                     Startup                            -
 ---------------------------------------------------------- */

$(document).ready(function () {
    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            applyKoBindings();
            // myApp.showPreloader(AppMessages.COMN_INF103);

        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Device ready event subscribe
document.addEventListener("deviceready", function () {

    checkGPSAvailability(); // start the gps check
    tracking();
    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    navigator.app.backHistory()
}


//Knockout binging initialization
function applyKoBindings() {
    myApp.showPreloader(AppMessages.COMN_INF102);
    koBinding = new LeaveRequestListViewModel();
    setUserData(koBinding);
    loadLeaveData(function (cb) {
        var data = cb.data;
        if (data.length <= 0) {
            myApp.hidePreloader();
            myApp.addNotification({
                title: 'We are sorry!',
                message: 'You have no leaves available at the movement.'
            });
        } else {
            ko.utils.arrayForEach(data, function (item) {
                koBinding.leaveData.push(item);
            });
            myApp.hidePreloader();
        }
    }, function (err) {

    }, function (tag) {

    });
    ko.applyBindings(koBinding);
}
/* ----------------------------------------------------------
 -                     View Models                        -
 ---------------------------------------------------------- */

function LeaveRequestListViewModel() {
    var self = this;
    self.userName = ko.observable();
    self.imageUrl = ko.observable();
    self.userId = ko.observable();
    self.leaveData = ko.observableArray([]);
    self.requestLeaveClick = function () {
        navigateTo('leave_request.html');
    }
    self.checkGPS = function () {
        checkAuthorization();
    }
}


/* ----------------------------------------------------------
 -                     Functions                          -
 ---------------------------------------------------------- */

function setUserData(koBinding) {
    var user_name = getValueL(KEY_USERNAME);
    var user_image = getValueL(KEY_USERIMAGE);
    var user_id = getValueL(KEY_USRID);
    koBinding.imageUrl(user_image);
    koBinding.userName(user_name);
    koBinding.userId(user_id);
}

//Get leave information
function loadLeaveData(cb, err, tag) {
    var url = getValueL(KEY_APIBASEURL);
    var userID = getValueL(KEY_USRID);
    url = url + "v2/leave/list/" + userID;
    var data = {};
    AjaxCall(url, data, 2000, "GET", cb, err, true, true);
}