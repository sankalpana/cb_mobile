/* ----------------------------------------------------------
 -                  Global Variables                      -
 ---------------------------------------------------------- */

//Knockout view model variable
var koBinding;
/* ----------------------------------------------------------
 -              Knockout Custom Bindings                  -
 ---------------------------------------------------------- */

//Knockout binding for Framework7 Radios/Checkboxes
ko.bindingHandlers.f7Chk = {
    init: function (element, valueAccessor) {
        $(element).on('change', function () {
            var observable = valueAccessor();
            //if (observable() !== this.checked)
            observable(this.checked);
        });
    },
    update: function (element, valueAccessor) {
        var observable = valueAccessor();
        //if ($(element).prop("checked") !== observable())
        $(element).prop("checked", observable());
    }
};

/* ----------------------------------------------------------
 -                      Events                            -
 ---------------------------------------------------------- */

//Device ready event subscribe
document.addEventListener("deviceready", function () {

    checkGPSAvailability(); // start the gps check
    tracking();
    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    navigator.app.backHistory()
}

/* ----------------------------------------------------------
 -                     Startup                            -
 ---------------------------------------------------------- */

//Startup
$(document).ready(function () {
    //TODO: Temp remove this
    //setValueL(KEY_APIBASEURL, "https://api.instabuggy.com/v1.0/");
    //--

    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            applyKoBindings();
        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Knockout binging initialization
function applyKoBindings() {
    koBinding = new SuperOutletViewModel();
    setUserData(koBinding);
    loadTaskList(function (cb) {
        var data = cb.data;
        if (data.length <= 0) {
            myApp.addNotification({
                title: 'We are sorry!',
                message: 'You have no tasks at the movement.'
            });
        } else {
            ko.utils.arrayForEach(data, function (item) {
                koBinding.taskData.push(item);
            });
        }
    }, function (err) {

    }, function (tag) {

    });
    ko.applyBindings(koBinding);
    check_Attendance(koBinding);
}

/* ----------------------------------------------------------
 -                     View Models                        -
 ---------------------------------------------------------- */

//Main ViewModel
function SuperOutletViewModel() {
    var self = this;
    self.userId = ko.observable();
    self.userType = ko.observable();
    self.displayUserType = ko.pureComputed(function () {
            return self.userType() == "0" ? "Sales Promoter" : "Sales Supervisor";
        }
    );
    self.activationID = ko.observable();
    self.activationName = ko.observable();
    self.activationOutlet = ko.observable();
    self.activationPromoter = ko.observable();
    self.taskData = ko.observableArray([]);
    self.checkedIn = ko.observable(false);
    self.checkedOut = ko.observable(false);
    self.attandaceTime = ko.observable('-:-');
    self.attandaceLabel = ko.observable('--');
    self.formattedAttendancce = ko.computed(function () {
        var time = getFormatedTime(self.attandaceTime());
        return time;
    });
    self.pageLoad = ko.observable();
    self.attandaceStatus = ko.observable();
    self.attandaceStatus.subscribe(function (checkedVal) {
        myApp.showPreloader(AppMessages.ITM_INF104);
        var action = checkedVal ? "1" : "0";
        if (!self.pageLoad()) {
            self.pageLoad(false);
            //myApp.showPreloader(AppMessages.COMN_INF102);
            getLocation(null, 'false', null, null)
                .then(function (position) {
                    if (!self.checkedIn() && !self.checkedOut()) {
                        //send checkin data
                        myApp.confirm('Do you want to check in?', '', function () {
                            processAndSendLocationData(position, self.activationID(), self.userId(), "1", function (cb) {
                                self.attandaceTime(cb.check_in_time);
                                self.attandaceLabel('CheckIn');
                                self.checkedIn(true);
                                self.attandaceStatus(true);
                            });
                        }, function () {
                            //self.attandaceStatus(false);
                            $('#checkIn').attr('checked', false);
                        });
                    } else if (self.checkedIn() && !self.checkedOut()) {
                        //send checkout data
                        myApp.confirm('Do you want to check out?', '', function () {
                            processAndSendLocationData(position, self.activationID(), self.userId(), "0", function (cb) {
                                self.attandaceTime(cb.check_out_time);
                                self.attandaceLabel('CheckOut');
                                self.attandaceStatus(false);
                                self.checkedIn(true);
                                self.checkedOut(true);
                                $("#checkIn").prop("disabled", true);
                            });
                        }, function () {
                            //self.attandaceStatus(true);
                            $('#checkIn').attr('checked', true);
                        });
                    } else {
                        self.attandaceStatus(true);
                    }
                    myApp.hidePreloader();

                })
                .fail(function (err) {
                    myApp.hidePreloader();
                    myApp.alert(thrownError, AppMessages.COMN_ERR101);
                });
        } else {
            getLocation(null, 'false', null, null)
                .then(function (position) {
                    if (!self.checkedIn() && !self.checkedOut()) {
                        //send checkin data
                        processAndSendLocationData(position, self.activationID(), self.userId(), "1", function (cb) {
                            self.attandaceTime(cb.check_in_time);
                            self.attandaceLabel('CheckIn');
                            self.checkedIn(true);
                            self.attandaceStatus(true);
                        });
                    } else if (self.checkedIn() && !self.checkedOut()) {
                        //send checkout data
                        processAndSendLocationData(position, self.activationID(), self.userId(), "0", function (cb) {
                            self.attandaceTime(cb.check_out_time);
                            self.attandaceLabel('checkout');
                            self.attandaceStatus(false);
                            self.checkedIn(true);
                            self.checkedOut(true);
                            $("#checkIn").prop("disabled", true);
                        });
                    } else {
                        self.attandaceStatus(true);
                    }
                    myApp.hidePreloader();

                })
                .fail(function (err) {
                    myApp.hidePreloader();
                    myApp.alert(thrownError, AppMessages.COMN_ERR101);
                });
        }
    }, this);
    self.taskClick = function (c, event) {
        if (c != null) {
            myApp.showPreloader(AppMessages.COMN_INF102);
            var taskObj = c;
            setValueL(KEY_TSKOBJ, JSON.stringify(taskObj));
            //Successful login
            setTimeout(function () {
                navigateTo("super-task.html");
            }, 200);
        } else {
            myApp.hidePreloader();
            myApp.alert(AppMessages.COMN_ERR100, AppMessages.COMN_ERR101);
        }
    }
    self.checkGPS = function () {
        //send checkin data
        myApp.confirm('Would you like to share your current location?', '', function () {
            myApp.showPreloader(AppMessages.COMN_INF102);
            checkGPSAvailability();
            sendLocation();
            myApp.hidePreloader();
            myApp.alert(AppMessages.COMN_INF108, AppMessages.COMN_INF105);
        }, function () {

        });
    }
    self.picClick = function () {
        navigateTo("super_pic.html");
    }
    self.navigatToTarget = function () {
        navigateTo('traget.html');
    }
}


/* ----------------------------------------------------------
 -                     Functions                          -
 ---------------------------------------------------------- */

function setUserData(koBinding) {
    var act_id = getValueL(KEY_ACTID);
    var act_outlet = getValueL(KEY_ACTOUTLET);
    var act_promoter = getValueL(KEY_PROMOTER);
    var user_type = getValueL(KEY_USRTYPE);
    var user_id = getValueL(KEY_USRID);
    koBinding.activationID(act_id);
    koBinding.activationOutlet(act_outlet);
    koBinding.activationPromoter(act_promoter);
    koBinding.userId(user_id);
    koBinding.userType(user_type);
}

//Get task information
function loadTaskList(cb, err, tag) {
    var url = getValueL(KEY_APIBASEURL);
    var actID = getValueL(KEY_ACTID);
    url = url + "v2/supervisor/tasks/" + actID;
    var data = {};
    AjaxCall(url, data, 2000, "GET", cb, err, true, true, undefined, tag);
}

function check_Attendance(vm) {
    getAttandace(function (cb) {
        myApp.hidePreloader();
        var data = cb.data[0];
        if (data.check_in_time != "00:00:00" && data.check_out_time != "00:00:00") {
            $("#checkIn").prop("disabled", true);
            vm.attandaceTime(data.check_out_time);
            vm.attandaceLabel('CheckOut');
            vm.checkedIn(true);
            vm.checkedOut(true);
        } else if (data.check_in_time == "00:00:00") {
            myApp.confirm('Would you like to checking for today?', '', function () {
                vm.pageLoad(true);
                vm.attandaceStatus(true);
            }, function () {
                vm.checkedIn(false);
                vm.checkedOut(false);
            });
        } else {
            vm.attandaceTime(data.check_in_time);
            vm.attandaceLabel('CheckIn');
            vm.checkedIn(true);
            vm.checkedOut(false);
            //vm.attandaceStatus(true);
            $('#checkIn').attr('checked', true);
        }
    }, function (err) {
        myApp.alert(thrownError, AppMessages.COMN_ERR101);
    })
}

function checkGPSAvailability() {
    cordova.plugins.diagnostic.isGpsLocationAvailable(function (available) {
        console.log("GPS location is " + (available ? "available" : "not available"));
        if (!available) {
            checkAuthorization();
        } else {
            console.log("GPS location is ready to use");
            // document.getElementById("#gps").classList.remove('fa-map-marker');
            $('#gps').removeClass('fa-ban');
            $('#gps').addClass('fa-map-marker');
        }
    }, function (error) {
        console.error("The following error occurred: " + error);
    });
}

//Get campaign location information
function getAttandace(cb, err) {
    var url = getValueL(KEY_APIBASEURL);
    var userID = getValueL(KEY_USRID);
    var actID = getValueL(KEY_ACTID);
    url = url + "v2/supervisor/attendance?user_id=" + userID + "&activation_id=" + actID;
    var data = {};
    AjaxCall(url, data, 2000, "GET", cb, err, true, true, undefined, null);
}

//Get the geo location via jQuery promise
var getLocation = function (callback, highAccur, timeout, maxAge) {
    //Deferred object for promise interface
    var deferred = new $.Deferred();

    if ($.isFunction(callback)) {
        deferred.then(callback);
    }

    navigator.geolocation.getCurrentPosition(
        function (position) {
            deferred.resolve(position);
        },
        function (err) {
            deferred.reject(err);
        },
        {enableHighAccuracy: highAccur}
    );

    //{ enableHighAccuracy: highAccur, timeout: timeout, maximumAge: maxAge }

    return deferred.promise();
};

//Process and send location data to the server
function processAndSendLocationData(position, actID, usrId, action, cb) {
    var result = null; // 0 - ideal , 1 - checked in, 2 checked - out
    var data = new LocationData();
    data.latitude = checkProperValue(position) ? checkAndGetValue(position.coords.latitude, 0) : null;
    data.longitude = checkProperValue(position) ? checkAndGetValue(position.coords.longitude, 0) : null;
    data.activation_id = actID;
    data.user_id = usrId;
    data.at_type = action;

    var url = getValueL(KEY_APIBASEURL) + "v2/supervisor/attendance";
    var params = data;
    var type = "POST";
    var timeOut = 2000;
    AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        if (data.meta.status == "true") {
            cb(data.data);
            // check out thing here
            myApp.hidePreloader();
            //myApp.alert(AppMessages.ITM_INF100,AppMessages.COMN_INF105);
        } else {
            myApp.hidePreloader();
            myApp.alert(thrownError, AppMessages.COMN_ERR101);
        }
    }, function (xhr, ajaxOptions, thrownError) {
        myApp.hidePreloader();
        myApp.alert(thrownError, AppMessages.COMN_ERR101);
    });
}

//Location data sent to the server
var LocationData = function () {

    /* Mandatory */

    //Latitude
    this.latitude = null;
    //Longitude
    this.longitude = null;
    //Activation ID
    this.activation_id = null;
    //User ID
    this.userId = null;
    //Action (Checking || checkout)
    this.at_type = null;

    /* Optional */

}


