/* ----------------------------------------------------------
 -                  Global Variables                      -
 ---------------------------------------------------------- */

//Knockout view model variable
var koBinding;

//This hold captured image data url till upload
var base64string = "";

var retryCount = 0;

/* ----------------------------------------------------------
 -              Knockout Custom Bindings                  -
 ---------------------------------------------------------- */

//Knockout binding for Framework7 Radios/Checkboxes
ko.bindingHandlers.f7Chk = {
    init: function (element, valueAccessor) {
        $(element).on('change', function () {
            var observable = valueAccessor();
            //if (observable() !== this.checked)
            observable(this.checked);
        });
    },
    update: function (element, valueAccessor) {
        var observable = valueAccessor();
        //if ($(element).prop("checked") !== observable())
        $(element).prop("checked", observable());
    }
};

/* ----------------------------------------------------------
 -                      Events                            -
 ---------------------------------------------------------- */

//Device ready event subscribe
document.addEventListener("deviceready", function () {

    checkGPSAvailability(); // start the gps check
    tracking();
    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    navigator.app.backHistory()
}

/* ----------------------------------------------------------
 -                     Startup                            -
 ---------------------------------------------------------- */

//Startup
$(document).ready(function () {
    //TODO: Temp remove this
    //setValueL(KEY_APIBASEURL, "https://api.instabuggy.com/v1.0/");
    //--

    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            applyKoBindings();
        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Knockout binging initialization
function applyKoBindings() {
    koBinding = new SuperPhotoViewModel();
    setValueL(KEY_RETRY_COUNT, 0);
    setUserData(koBinding);
    ko.applyBindings(koBinding);
    checkIfImageExists();
}

/* ----------------------------------------------------------
 -                     View Models                        -
 ---------------------------------------------------------- */

//Main ViewModel
function SuperPhotoViewModel() {
    var self = this;
    self.userId = ko.observable();
    self.userType = ko.observable();
    self.activationID = ko.observable();
    self.activationName = ko.observable();
    self.activationOutlet = ko.observable();
    self.activationPromoter = ko.observable();
    self.uploadClick = function (c, event) {
        myApp.showPreloader(AppMessages.IMAGE_INF100, "");
        uploadImage();
    }
    self.checkGPS = function () {
        //send checkin data
        myApp.confirm('Would you like to share your current location?', '', function () {
            myApp.showPreloader(AppMessages.COMN_INF102);
            checkGPSAvailability();
            sendLocation();
            myApp.hidePreloader();
            myApp.alert(AppMessages.COMN_INF108, AppMessages.COMN_INF105);
        }, function () {

        });
    }
    self.capturePhoto = function () {
        //making sure previous image removed.
        base64string = "";
        var retryCount = 0;
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 20, allowEdit: false, saveToPhotoAlbum: false,
            destinationType: Camera.DestinationType.DATA_URL
        });
    }
}


/* ----------------------------------------------------------
 -                     Functions                          -
 ---------------------------------------------------------- */

function setUserData(koBinding) {
    var act_id = getValueL(KEY_ACTID);
    var act_outlet = getValueL(KEY_ACTOUTLET);
    var act_promoter = getValueL(KEY_PROMOTER);
    var user_type = getValueL(KEY_USRTYPE);
    var user_id = getValueL(KEY_USRID);
    koBinding.activationID(act_id);
    koBinding.activationOutlet(act_outlet);
    koBinding.activationPromoter(act_promoter);
    koBinding.userId(user_id);
    koBinding.userType(user_type);
}

function checkGPSAvailability() {
    cordova.plugins.diagnostic.isGpsLocationAvailable(function (available) {
        console.log("GPS location is " + (available ? "available" : "not available"));
        if (!available) {
            checkAuthorization();
        } else {
            console.log("GPS location is ready to use");
            // document.getElementById("#gps").classList.remove('fa-map-marker');
            $('#gps').removeClass('fa-ban');
            $('#gps').addClass('fa-map-marker');
        }
    }, function (error) {
        console.error("The following error occurred: " + error);
    });
}

//Get the geo location via jQuery promise
var getLocation = function (callback, highAccur, timeout, maxAge) {
    //Deferred object for promise interface
    var deferred = new $.Deferred();

    if ($.isFunction(callback)) {
        deferred.then(callback);
    }

    navigator.geolocation.getCurrentPosition(
        function (position) {
            deferred.resolve(position);
        },
        function (err) {
            deferred.reject(err);
        },
        {enableHighAccuracy: highAccur}
    );

    //{ enableHighAccuracy: highAccur, timeout: timeout, maximumAge: maxAge }

    return deferred.promise();
};

function taskResult(sliderVal, remarks) {
    myApp.showPreloader();
    sendTaskResult(sliderVal, remarks, function (success) {
        myApp.hidePreloader();
        myApp.alert(AppMessages.TASK_INF100, AppMessages.COMN_INF105);
    }, function (err) {
        myApp.hidePreloader();
        myApp.alert(AppMessages.COMN_ERR101, AppMessages.COMN_ERR100);
    });

}

function sendTaskResult(siderVal, remarks, scb, err) {
    var url = getValueL(KEY_APIBASEURL) + "v2/supervisor/task";
    var userId = getValueL(KEY_USRID);
    var actId = getValueL(KEY_ACTID);
    var tID = getValueL(KEY_TSKID);
    var params = {"user_id": userId, "activation_id": actId, "task_id": tID, "rating": siderVal, "remark": remarks};
    var type = "POST";
    var timeOut = 2000;
    AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        if (data.meta.status == "true") {
            cb(data.data);
        } else {
            err(thrownError);
        }
    }, function (xhr, ajaxOptions, thrownError) {
        err(thrownError);
    });
}


function onSuccess(imageData) {
    var image = document.getElementById('userImage');
    image.src = "data:image/jpeg;base64," + imageData;
    base64string = "data:image/jpeg;base64," + imageData;
}

function onFail(message) {
    alert('Failed because: ' + message);
}

function uploadImage() {
    if (checkProperValue(base64string)) {
        var url = getValueL(KEY_APIIMAGEUPLOADURL);
        var params = {"encode_data": base64string};
        var type = "POST";
        var timeOut = 0;

        AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
            if (data.meta.status == "true" || data.meta.status == true) {
                sendImageUrl(data.data.img_url);
                // check out thing here
                myApp.hidePreloader();
                myApp.alert(AppMessages.IMAGE_INF101, AppMessages.COMN_INF105);
            } else {
                myApp.hidePreloader();
                myApp.confirm('Image upload failed. Would you like to retry?', '', function () {
                    myApp.showPreloader(AppMessages.IMAGE_INF100, "");
                    uploadImage();
                }, function () {

                });

                // if(retryCount < 5){
                //     retryCount++;
                //     //setValueL(KEY_RETRY_COUNT, getValueL(KEY_RETRY_COUNT)+1);
                //     uploadImage();
                // }else{
                //     myApp.hidePreloader();
                //     myApp.alert(AppMessages.COMN_ERR101,"Error");
                // }
            }
        }, function (xhr, ajaxOptions, thrownError) {
            myApp.hidePreloader();
            myApp.confirm('Image upload failed. Would you like to retry?', '', function () {
                myApp.showPreloader(AppMessages.IMAGE_INF100, "");
                uploadImage();
            }, function () {

            });
            // if(retryCount < 5){
            //     retryCount++;
            //     uploadImage();
            // }else{
            //     myApp.hidePreloader();
            //     myApp.alert(AppMessages.COMN_ERR101, AppMessages.COMN_ERR100);
            // }
        });

    } else {
        myApp.hidePreloader();
        myApp.confirm('Image upload failed. Would you like to retry?', '', function () {
            myApp.showPreloader(AppMessages.IMAGE_INF100, "");
            uploadImage();
        }, function () {

        });
        // if(getValueL(KEY_RETRY_COUNT) > 5){
        //     uploadImage();
        // }else{
        //     myApp.hidePreloader();
        //     myApp.alert(AppMessages.COMN_ERR101, AppMessages.COMN_ERR100);
        // }
        //myApp.alert(AppMessages.COMN_ERR100, AppMessages.COMN_ERR101);
    }
}

function sendImageUrl(Imgurl) {
    var url = getValueL(KEY_APIBASEURL) + "v2/supervisor/summary";
    var userId = getValueL(KEY_USRID);
    var actId = getValueL(KEY_ACTID);
    var params = {"user_id": userId, "activation_id": actId, "image": Imgurl};
    var type = "POST";
    var timeOut = 0;
    AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        var image = document.getElementById('userImage');
        image.src = Imgurl;
        $("#picUpload").addClass("disable_a_href");
        $(".buddy-img").addClass("disable_a_href");
    }, function (xhr, ajaxOptions, thrownError) {
        myApp.hidePreloader();
        myApp.alert(AppMessages.COMN_ERR101, AppMessages.COMN_ERR100);
    });
}

function checkIfImageExists() {
    var actId = getValueL(KEY_ACTID);
    var url = getValueL(KEY_APIBASEURL) + "v2/supervisor/summary/" + actId;
    var params = {};
    var type = "GET";
    var timeOut = 3000;
    AjaxCall(url, params, timeOut, type, function (data, textStatus, jqXHR) {
        if (data.meta.status == true || data.meta.status == "true") {
            document.getElementById('userImage').src = data.data[0].image;
            $("#picUpload").addClass("disable_a_href");
            $(".buddy-img").addClass("disable_a_href");
        } else {
            $("#picUpload").removeClass("disable_a_href");
            $(".buddy-img").removeClass("disable_a_href");
        }
    }, function (xhr, ajaxOptions, thrownError) {
        myApp.hidePreloader();
        myApp.alert(AppMessages.COMN_ERR101, AppMessages.COMN_ERR100);
    });
}