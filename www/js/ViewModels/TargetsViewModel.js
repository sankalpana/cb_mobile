/* ----------------------------------------------------------
 -                  Global Variables                      -
 ---------------------------------------------------------- */

var koBinding;

/* ----------------------------------------------------------
 -              Knockout Custom Bindings                  -
 ---------------------------------------------------------- */

/* ----------------------------------------------------------
 -                     Startup                            -
 ---------------------------------------------------------- */

$(document).ready(function () {
    //Get the app localization file and add it to window.AppMessages object
    loadMessage("en-CA",
        function (r) {
            console.log("localization loaded: " + r);
            applyKoBindings();
            // myApp.showPreloader(AppMessages.COMN_INF103);

        },
        function (err) {
            console.error("Failed to load locale: " + err);
        });
});

//Device ready event subscribe
document.addEventListener("deviceready", function () {
    tracking();
    //Device back button press event subscribe
    document.addEventListener("backbutton", deviceBackButtonPress, false);
}, true);


//Device back button press event handler
function deviceBackButtonPress() {
    //override the device back button to navigate to previous inline page
    navigator.app.backHistory()
}

function applyKoBindings() {
    myApp.showPreloader(AppMessages.COMN_INF102);
    koBinding = new TargetsViewModel();
    setSummeryData(koBinding);
    loadTargets(koBinding);
    ko.applyBindings(koBinding);
}

/* ----------------------------------------------------------
 -                     View Models                        -
 ---------------------------------------------------------- */

function TargetsViewModel() {
    var self = this;
    self.usrId = ko.observable();
    self.activationId = ko.observable();
    self.activationName = ko.observable();
    self.activationOutlet = ko.observable();
    self.targetData = ko.observableArray([]);
    self.overAll = ko.observable();
    self.avarage = ko.observable();
    self.totalSale = ko.observable();
    self.totalTarget = ko.observable();
}

function TargetsDataViewModel(data) {
    var self = this;
    self.id = ko.observable(data.id);
    self.brandName = ko.observable(data.brand_name);
    self.target = ko.observable(data.target);
    self.targetFormatted = ko.computed(function () {
        return 'Target: ' + self.target();
    });
    self.achievement = ko.observable(data.achievement);
    self.achievementFormatted = ko.computed(function () {
        return 'Achievement: ' + self.achievement();
    });
    self.percentage = ko.computed(function () {
        var val = parseFloat(self.achievement()) / parseFloat(self.target()) * 100;
        return val.toFixed(0) + '%';
    });
}

/* ----------------------------------------------------------
 -                     Functions                          -
 ---------------------------------------------------------- */

function setSummeryData(koBinding) {
    var uid = getValueL(KEY_USRID);
    var actId = getValueL(KEY_ACTID);
    var actName = getValueL(KEY_ACTNAME);
    var actOutlet = getValueL(KEY_ACTOUTLET);
    koBinding.usrId(uid);
    koBinding.activationId(actId);
    koBinding.activationName(actName);
    koBinding.activationOutlet(actOutlet);
}

function loadTargets(koBinding) {
    myApp.showIndicator();
    getTargets(
        function (cb) {
            if (cb.meta.status == 'true') {
                koBinding.overAll(cb.data.overall);
                koBinding.avarage(cb.data.avarage);
                koBinding.totalSale(cb.data.total_sale);
                koBinding.totalTarget(cb.data.total_target);
                var data = cb.data.target;
                if (data.length <= 0) {
                    myApp.hideIndicator();
                    myApp.hidePreloader();
                    myApp.alert(AppMessages.SALE_INF100, AppMessages.COMN_INF107);
                } else {
                    for (var i = 0; i < data.length; i++) {
                        var myObj = new TargetsDataViewModel(data[i]);
                        koBinding.targetData.push(myObj);
                    }
                    myApp.hideIndicator();
                    myApp.hidePreloader();
                }
            }
            myApp.hideIndicator();
            myApp.hidePreloader();
        },
        function (err) {
            myApp.hideIndicator();
            myApp.hidePreloader();
            myApp.alert(err.responseText, AppMessages.COMN_ERR100);
        });
}

function getTargets(cb, err) {
    var actID = getValueL(KEY_ACTID);
    var url = getValueL(KEY_APIBASEURL) + "v2.1/sales/target/" + actID;
    var type = "GET";
    var timeOut = 2000;
    AjaxCall(url, {}, timeOut, type, cb, err, true, true);
}